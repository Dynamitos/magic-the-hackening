#include "cam.h"

#define PI 3.14159265359
#define toRadians(x) x*PI/180

Camera::Camera(Player * player, float& deltaX, float& deltaY, double& rotation)
	:position{ glm::vec3(0, 1, 0) }, player{ player }, distanceFromPlayer{ 50 }, yaw{ 180 - angleAroundPlayer } {
	this->deltaX = &deltaX;
	this->deltaY = &deltaY;
	this->rotation = &rotation;
}


void Camera::calculateCameraPosition(float horizontalDistance, float verticalDistance)
{
	float theta = angleAroundPlayer;
	float xOffset = (float)(horizontalDistance * sin(toRadians(theta)));
	float zOffset = (float)(horizontalDistance * cos(toRadians(theta)));
	position.x = player->getPosition().x - xOffset;
	position.z = player->getPosition().z - zOffset;
	position.y = player->getPosition().y + verticalDistance;
	//TODO add terrain calculation
}

float Camera::calculateHorizontalDistance()
{
	return (float)(distanceFromPlayer * cos(toRadians(pitch)));
}

float Camera::calculateVerticalDistance()
{
	return (float)(distanceFromPlayer * sin(toRadians(pitch)));
}

void Camera::calculateZoom()
{
	float zoomLevel = (float)(*this->rotation * 2);
	*this->rotation = 0;
	distanceFromPlayer -= zoomLevel;
	if (distanceFromPlayer < 2)
		distanceFromPlayer = 2;
}

void Camera::calculatePitch()
{
	float pitchChange = *this->deltaY * 0.3;
	pitch += pitchChange;
	if (pitch > 90)
		pitch = 90;
	if (pitch < -90)
		pitch = -90;
}

void Camera::calculateAngleAroundPlayer()
{
	float angleChange = *this->deltaX * 0.3;
	angleAroundPlayer -= angleChange;
	yaw += angleChange;
	player->rotY -= angleChange;
}

void Camera::move()
{
	calculateAngleAroundPlayer();
	calculatePitch();
	calculateZoom();
	float horizontalDistance = calculateHorizontalDistance();
	float verticalDistance = calculateVerticalDistance();
	calculateCameraPosition(horizontalDistance, verticalDistance);

}
