#ifndef CAMERA_H
#define CAMERA_H
#include <math.h>
#include "player.h"


class Camera
{
public:
	Camera(Player* player, float& deltaX, float& deltaY, double& rotation);

	glm::vec3 position;
	float pitch;
	float yaw;

	void move();
	Player* getPlayer() { return player; }
	void invertPitch() { pitch = -pitch; }
	void setY(float y) { position.y = y; }
private:
	float distanceFromPlayer;
	float angleAroundPlayer;
	float roll;
	Player* player;
	float terrainHeigth;
	float* deltaX;
	float* deltaY;
	double* rotation;

	void calculateCameraPosition(float horizontalDistance, float verticalDistance);
	float calculateHorizontalDistance();
	float calculateVerticalDistance();
	void calculateZoom();
	void calculatePitch();
	void calculateAngleAroundPlayer();
};

#endif /* !CAMERA_H */