#ifndef DATA_H
#define DATA_H
#include <vector>
#include "util.h"
class Loader;
class Spell;
extern std::vector<Spell*> activeSpells;
class Data
{
public:
	static void loadResources(Loader* loader);
};
#endif /* !DATA_H */