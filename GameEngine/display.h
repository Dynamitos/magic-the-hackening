#ifndef DISPLAY_H
#define DISPLAY_H
#include <GLFW\glfw3.h>
#include "util.h"
#include "spell.h"
static int CLOSE_KEY = GLFW_KEY_ESCAPE;
static int JUMP_KEY = GLFW_KEY_SPACE;
static int FORWARD_KEY = GLFW_KEY_W;
static int BACKWARD_KEY = GLFW_KEY_S;
static int LEFT_KEY = GLFW_KEY_A;
static int RIGHT_KEY = GLFW_KEY_D;
static int SPELL_KEY = GLFW_KEY_E;


static GLFWwindow* window;
//static Input input;
//static MouseInput mouseWheel;
static float deltaX, deltaY;
static double rotation;
static bool buffering;
static int const WIDTH = 3840 / 4;
static int const HEIGHT = 2160 / 4;
static double lastFrameTime;
static GLfloat delta;
static double newX;
static double newY;
static bool mouseLocked;
static float mouseSpeed;
static bool keys[65536];
static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
	rotation = yoffset;
}
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == -1)
		return;
	keys[key] = action != GLFW_RELEASE;
}

static void updateMouse() {
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS) {
		glfwSetCursorPos(window, WIDTH / 2, HEIGHT / 2);
		mouseLocked = true;
		//TODO add MasterRenderer shooting
	}
	else {
		//TODO add MasterRenderer shooting
	}
	if (mouseLocked) {
		double x, y;
		glfwGetCursorPos(window, &x, &y);
		newX = x;
		newY = y;
		deltaX = (float)(newX - (WIDTH / 2))*mouseSpeed;
		deltaY = (float)(newY - (HEIGHT / 2))*mouseSpeed;
		glfwSetCursorPos(window, WIDTH / 2, HEIGHT / 2);
	}
}
static void createDisplay() {
	newX = WIDTH / 2;
	newY = HEIGHT / 2;
	mouseLocked = false;
	mouseSpeed = 0.1f;
	buffering = false;
	glewExperimental = true;
	if (!glfwInit()) {
		std::cout << "FAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACK" << std::endl;
	}
	glfwWindowHint(GLFW_SAMPLES, 4);
	window = glfwCreateWindow(WIDTH, HEIGHT, "FINALLY", NULL, NULL);
	glfwShowWindow(window);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
	lastFrameTime = glfwGetTime();
	currentTime = lastFrameTime;
	glewInit();
	glfwSetKeyCallback(window, key_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}
static void updateDisplay() {
	glfwPollEvents();
	updateMouse();
	glfwSwapBuffers(window);
	double currentFrameTime = glfwGetTime();
	delta = (float)(currentFrameTime - lastFrameTime);
	lastFrameTime = currentFrameTime;
	currentTime += delta;
}
static void closeDisplay() {
	glfwDestroyWindow(window);
	glfwTerminate();
}
static float getFrameTimeSeconds() {
	return delta;
}
#endif /* !DISPLAY_H */