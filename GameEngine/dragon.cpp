#include "dragon.h"

Dragon::Dragon(TexturedModel* model, glm::vec3 position, GLfloat rotX, GLfloat rotY, GLfloat rotZ, GLfloat scale, Entity* player, Status* status)
	:EntityMob{ model, position, rotX, rotY, rotZ, scale, status },
	RUN_SPEED{ 30 }, GRAVITY{ 0 }, JUMP_POWER{ 10 },
	player{ player }

{
	setPreviousDirection(player->getPosition() - getPosition());
}

Dragon::~Dragon()
{}

void Dragon::move(float frameTime)
{
	if (glm::length(position - player->getPosition()) >= 100)
	{
		 setPreviousDirection(player->getPosition() - getPosition());
	}
	glm::vec3 direction = getPreviousDirection();
	glm::normalize(direction);
	direction *= RUN_SPEED * frameTime;

	Entity::move(direction.x, direction.y, direction.z);
}