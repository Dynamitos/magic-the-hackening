#ifndef DRAGON_H
#define DRAGON_H

#include "entitymob.h"
#include <stdlib.h>

class Dragon : public EntityMob
{
	public:
		Dragon(TexturedModel* model, glm::vec3 position, GLfloat rotX, GLfloat rotY, GLfloat rotZ, GLfloat scale, Entity* player, Status* status);
		~Dragon();
		void move(float frameTime);
	private:
		float RUN_SPEED;
		float GRAVITY;
		float JUMP_POWER;
		Entity* player;
};

#endif /*!DRAGON_H*/
