#include "element.h"

Element::Element(EntityMob* caster, Type* type, float windUpTime, float duration)
	:_type(type), _caster(caster), _windUpTime(windUpTime), _remainingWindUp(windUpTime)
{}


Element::~Element()
{
	delete _type;
	delete _caster;
}

Type* Element::getType()
{
	return _type;
}

std::vector<Particle> Element::invoke(float delta)
{
	_remainingWindUp -= delta;
	if (_remainingWindUp <= 0)
	{
		return std::vector<Particle>();
	}
	std::vector<Particle> particles;
	for (int i = 0; i < _caster->status->INTENSITY *delta; i++)
	{
		Particle p = Particle();
		p.a = a;
		p.r = r;
		p.g = g;
		p.b = b;
		glm::vec3 direction = glm::normalize(glm::vec3((float)rand() / RAND_MAX * 2 - 1, (float)rand() / RAND_MAX * 2 - 1, (float)rand() / RAND_MAX * 2 - 1));
		direction *= 10;
		particles.push_back(p);
	}
	return particles;
}

Heat::Heat(EntityMob* caster, Type* type, float windUpTime, float duration)
	:Element{ caster, type, windUpTime, duration }
{
	Element::r = 0;
	Element::g = 0;
	Element::b = 0;
	Element::a = 1;
}

Water::Water(EntityMob* caster, Type* type, float windUpTime, float duration)
	: Element{ caster, type, windUpTime, duration }
{
	Element::r = 0;
	Element::g = 0;
	Element::b = 0;
	Element::a = 1;
}

Metal::Metal(EntityMob* caster, Type* type, float windUpTime, float duration)
	: Element{ caster, type, windUpTime, duration }
{
	Element::r = 0;
	Element::g = 0;
	Element::b = 0;
	Element::a = 1;
}

Light::Light(EntityMob* caster, Type* type, float windUpTime, float duration)
	: Element{ caster, type, windUpTime, duration }
{
	Element::r = 0;
	Element::g = 0;
	Element::b = 0;
	Element::a = 1;
}

Darkness::Darkness(EntityMob* caster, Type* type, float windUpTime, float duration)
	: Element{ caster, type, windUpTime, duration }
{
	Element::r = 0;
	Element::g = 0;
	Element::b = 0;
	Element::a = 1;
}

Wind::Wind(EntityMob* caster, Type* type, float windUpTime, float duration)
	: Element{ caster, type, windUpTime, duration }
{
	Element::r = 0;
	Element::g = 0;
	Element::b = 0;
	Element::a = 1;
}

Cold::Cold(EntityMob* caster, Type* type, float windUpTime, float duration)
	: Element{ caster, type, windUpTime, duration }
{
	Element::r = 0;
	Element::g = 0;
	Element::b = 0;
	Element::a = 1;
}

Glass::Glass(EntityMob* caster, Type* type, float windUpTime, float duration)
	: Element{ caster, type, windUpTime, duration }
{
	Element::r = 0;
	Element::g = 0;
	Element::b = 0;
	Element::a = 1;
}