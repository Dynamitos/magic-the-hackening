#ifndef ELEMENT_H
#define ELEMENT_H

#include "type.h"
#include <flex.h>

class EntityMob;

struct Particle
{
	glm::vec3 pos, speed, source, destination;
	GLbyte r = 1, g = 0, b = 0, a = 1;
	GLfloat width = 1, height = 1;
	float mass;
	float life;
	int phase = flexMakePhase(0, eFlexPhaseSelfCollide | eFlexPhaseFluid);
};

class Element
{
public:
	Element(EntityMob* caster, Type* type, float windUpTime, float duration);
	~Element();
	Type* getType();
	std::vector<Particle> invoke(float delta);
	unsigned char r, g, b, a;
private:
	Type* _type;
	float _windUpTime, _remainingWindUp;
	float _duration;
	EntityMob* _caster;
};

class Heat : public Element
{
public:
	Heat(EntityMob* caster, Type* type, float windUpTime, float duration);
private:
};

class Water : public Element
{
public:
	Water(EntityMob* caster, Type* type, float windUpTime, float duration);
private:
};

class Metal : public Element
{
public:
	Metal(EntityMob* caster, Type* type, float windUpTime, float duration);
private:
};

class Light : public Element
{
public:
	Light(EntityMob* caster, Type* type, float windUpTime, float duration);
private:
};

class Darkness : public Element
{
public:
	Darkness(EntityMob* caster, Type* type, float windUpTime, float duration);
private:
};

class Wind : public Element
{
public:
	Wind(EntityMob* caster, Type* type, float windUpTime, float duration);
private:
};

class Cold : public Element
{
public:
	Cold(EntityMob* caster, Type* type, float windUpTime, float duration);
private:
};

class Glass : public Element
{
public:
	Glass(EntityMob* caster, Type* type, float windUpTime, float duration);
private:
};
#endif /*!ELEMENT_H*/
