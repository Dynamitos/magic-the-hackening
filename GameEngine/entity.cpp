#include "entity.h"

Entity::Entity(TexturedModel * model, glm::vec3 position, float rotX, float rotY, float rotZ, float scale)
	:model{ model }, position{ position }, rotX{ rotX }, rotY{ rotY }, rotZ{ rotZ }, scale{ scale } {
}
Entity::Entity(TexturedModel * model, int index, glm::vec3 position, float rotX, float rotY, float rotZ, float scale)
	: model{ model }, textureIndex{ index }, position {	position }, rotX{ rotX }, rotY{ rotY }, rotZ{ rotZ }, scale{ scale } {
}

void Entity::move(float dx, float dy, float dz)
{
	//int currentX = (int)(position.x / Terrain.SIZE);
	//int currentZ = (int)(position.z / Terrain.SIZE);
	position.x += dx;
	position.y += dy;
	position.z += dz;
	//int newX = (int)(position.x / Terrain.SIZE);
	//int newZ = (int)(position.z / Terrain.SIZE);
	//if (newX != currentX || newZ != currentZ) {
	//	Data.chunks.onEntityTerrainChange(currentX, currentZ, newX, newZ, this);
	//}
}

void Entity::rotate(float dx, float dy, float dz)
{
	rotX += dx;
	rotY += dy;
	rotZ += dz;
}
