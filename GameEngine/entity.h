#ifndef ENTITY_H
#define ENTITY_H
#include <glm\glm.hpp>
#include <GL\glew.h>
class TexturedModel;

class Entity
{
public:
	Entity(TexturedModel* model, glm::vec3 position, float rotX, float rotY, float rotZ, float scale);
	Entity(TexturedModel* model, int textureIndex, glm::vec3 position, float rotX, float rotY, float rotZ, float scale);
	void move(float dx, float dy, float dz);
	void rotate(float dx, float dy, float dz);
	TexturedModel* getModel() { return model; }
	glm::vec3 getPosition() { return position; }
	float rotX, rotY, rotZ;
	float scale;
	int textureIndex;
	TexturedModel* model;
	glm::vec3 position;
};
#endif /* !ENTITY_H */