#include "entitymob.h"

EntityMob::EntityMob(TexturedModel* model, glm::vec3 position, GLfloat rotX, GLfloat rotY, GLfloat rotZ, GLfloat scale, Status* status,	glm::vec3 catalyst)
	:Entity{ model, position, rotX, rotY, rotZ, scale }, status{ status }, prevDir(NULL)
{}

void EntityMob::decreaseIntensity(float value)
{
	status->INTENSITY -= value;
	if (status->INTENSITY < 0)
		status->INTENSITY = 0;
}

void EntityMob::decreaseDamage(float value)
{
	status->DAMAGE -= value;
	if (status->DAMAGE < 0)
		status->DAMAGE = 0;
}

void EntityMob::decreaseHP(float value)
{
	status->current_hp -= value;
	if (status->current_hp <= 0)
		status->isAlive = false;
}

void EntityMob::increaseIntensity(float value)
{
	status->INTENSITY += value;
}

void EntityMob::increaseHP(float value)
{
	if (status->current_hp + value >= status->HP)
		return;
	status->current_hp += value;
}

void EntityMob::increaseDamage(float value)
{
	status->DAMAGE += value;
}

void EntityMob::setPreviousDirection(glm::vec3 prevDirection)
{
	prevDir = prevDirection;
}

glm::vec3 EntityMob::getPreviousDirection()
{
	return prevDir;
}