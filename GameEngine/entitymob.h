#ifndef ENTITYMOB_H
#define ENTITYMOB_H

#include "entity.h"
#include "status.h"
#include <vector>
class Spell;

class EntityMob : public Entity
{
public:
	EntityMob(TexturedModel* model, glm::vec3 position, GLfloat rotX, GLfloat rotY, GLfloat rotZ, GLfloat scale, Status* status, glm::vec3 catalyst);
	void decreaseHP(float value);
	void decreaseDamage(float value);
	void decreaseIntensity(float value);

	void increaseHP(float value);
	void increaseDamage(float value);
	void increaseIntensity(float value);
	
	void setPreviousDirection(glm::vec3 prevDirection);
	glm::vec3 getPreviousDirection();

	Status* status;
private:
	glm::vec3 prevDir;
};


#endif /*!ENTITYMOB_H*/