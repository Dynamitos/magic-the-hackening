#include "entityrenderer.h"

EntityRenderer::EntityRenderer(glm::mat4 projectionMatrix) {
	shader = new EntityShader();
	shader->initShader();
	shader->start();
	shader->loadProjectionMatrix(projectionMatrix);
	shader->stop();
	matrices = (glm::mat4*)malloc(sizeof(glm::mat4)*MAX_BONES);
}

EntityRenderer::~EntityRenderer()
{
	delete shader;
}

void EntityRenderer::render(Camera* camera, std::multimap<TexturedModel*, Entity*> entities, glm::vec4 clipPlane)
{
	shader->start();
	shader->loadSkyColor(SKY_COLOR);
	shader->loadViewMatrix(camera);
	shader->loadFog(gradient, density);
	shader->loadDisplacement(1);
	shader->loadPlane(clipPlane);
	shader->loadTextures();
	std::pair <std::multimap<TexturedModel*, Entity*>::iterator,
		std::multimap<TexturedModel*, Entity*>::iterator> range;
	for (std::multimap<TexturedModel*, Entity*>::iterator i = entities.begin(); i != entities.end(); i = range.second)
	{
		range = entities.equal_range(i->first);
		TexturedModel* model = range.first->first;
		for (int i = 0; i < model->getNumModels(); i++) {
			prepareTexturedModel(model, i);
			for (std::multimap<TexturedModel*, Entity*>::iterator d = range.first; d != range.second; ++d) {
				prepareInstance(d->second);
				glDrawElements(GL_PATCHES, model->getModels()[i]->getVertexCount(), GL_UNSIGNED_INT, 0);
			}
			unbindTexturedModel();
		}
	}
	shader->stop();
}

void EntityRenderer::prepareTexturedModel(TexturedModel* model, GLuint meshNumber)
{
	GLuint numBones = model->update(matrices);
	shader->loadBones(matrices, numBones);//TODO remove
	RawModel* rawModel = model->getModels()[meshNumber];
	glBindVertexArray(rawModel->getVaoID());
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	ModelTexture* texture = model->getTextures()[0];//rawModel->getMaterialIndex()];//TODO REMOVE!!!!!
	if (texture->hasTransparency)
		disableCulling();
	shader->loadFakeLighting(texture->useFakeLighting);
	shader->loadShineVariables(texture->shineDamper, texture->reflectivity);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture->diffuseID);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texture->displacementID);

}
void EntityRenderer::unbindTexturedModel()
{
	enableCulling();
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(4);
	glBindVertexArray(0);
}
void EntityRenderer::prepareInstance(Entity* entity)
{
	glm::mat4 transformationMatrix = createTransformationMatrix(entity->getPosition(), entity->rotX, entity->rotY, entity->rotZ, entity->scale);
	shader->loadTransformationMatrix(transformationMatrix);
}

