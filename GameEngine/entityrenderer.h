#ifndef ENTITYRENDERER_H
#define ENTITYRENDERER_H
#include <glm\glm.hpp>
#include <map>
#include "texmodel.h"
#include "entityshader.h"
class EntityRenderer
{
public:
	EntityRenderer(glm::mat4 projectionMatrix);
	~EntityRenderer();
	void render(Camera* camera, std::multimap<TexturedModel*, Entity*> entities, glm::vec4 clipPlane);
private:
	void prepareTexturedModel(TexturedModel* model, GLuint meshNumber);
	void unbindTexturedModel();
	void prepareInstance(Entity* entity);
	glm::mat4* matrices;
	EntityShader* shader;
};
#endif /* !ENTITYRENDERER_H */