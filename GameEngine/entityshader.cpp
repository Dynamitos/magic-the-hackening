#include "entityshader.h"

EntityShader::EntityShader() 
	:ShaderProgram("entityVertex.shader", "entityFragment.shader", "entityTesselationControl.shader", "entityTesselationEvaluation.shader")
{

}
EntityShader::~EntityShader()
{

}
void EntityShader::loadDisplacement(GLfloat displacementFactor)
{
	loadFloat(location_displacementFactor, displacementFactor);
}
void EntityShader::loadOffset(GLfloat x, GLfloat y)
{
	loadVector(location_offset, x, y);
}
void EntityShader::loadFog(GLfloat gradient, GLfloat density)
{
	loadFloat(location_gradient, gradient);
	loadFloat(location_density, density);
}
void EntityShader::loadSkyColor(glm::vec3 skyColor)
{
	loadVector(location_skyColor, skyColor);
}
void EntityShader::loadFakeLighting(GLboolean useFake)
{
	loadBoolean(location_useFakeLighting, useFake);
}
void EntityShader::loadShineVariables(GLfloat damper, GLfloat reflectivity)
{
	loadFloat(location_shineDamper, damper);
	loadFloat(location_reflectivity, reflectivity);
}
void EntityShader::loadViewMatrix(Camera * camera)
{
	loadVector(location_cameraPos, camera->position);
	loadMatrix(location_viewMatrix, createViewMatrix(camera));
}
void EntityShader::loadTransformationMatrix(glm::mat4 matrix)
{
	loadMatrix(location_transformationMatrix, matrix);
}
void EntityShader::loadProjectionMatrix(glm::mat4 projectionMatrix)
{
	loadMatrix(location_projectionMatrix, projectionMatrix);
}
void EntityShader::loadPlane(glm::vec4 plane)
{
	loadVector(location_plane, plane);
}
void EntityShader::loadLights()
{
	for (int i = 0; i < MAX_LIGHTS; i++)
	{
		if (i < sizeof(lights)/sizeof(Light*))
		{
			loadVector(location_lightColor[i], lights[i]->color);
			loadVector(location_lightPosition[i], lights[i]->position);
			loadVector(location_attenuation[i], lights[i]->attenuation);
		}
		else {
			loadVector(location_lightColor[i], glm::vec3(0, 0, 0));
			loadVector(location_lightPosition[i], glm::vec3(0, 0, 0));
			loadVector(location_attenuation[i], glm::vec3(1, 0, 0));
		}
	}
}
void EntityShader::loadTextures()
{
	loadInt(location_textureSampler, 0);
	loadInt(location_displacementSampler, 1);
}
void EntityShader::loadBones(glm::mat4 * matrices, GLuint bones)
{
	for (int i = 0; i < bones; i++)
	{
		loadMatrix(location_bones[i], matrices[i]);
	}
}
void EntityShader::getAllUniformLocations()
{
	location_transformationMatrix = ShaderProgram::getUniformLocation("transformationMatrix");
	location_projectionMatrix = ShaderProgram::getUniformLocation("projectionMatrix");
	location_viewMatrix = ShaderProgram::getUniformLocation("viewMatrix");
	location_reflectivity = ShaderProgram::getUniformLocation("reflectivity");
	location_shineDamper = ShaderProgram::getUniformLocation("shineDamper");
	location_useFakeLighting = ShaderProgram::getUniformLocation("useFakeLighting");
	location_skyColor = ShaderProgram::getUniformLocation("skyColor");
	location_density = ShaderProgram::getUniformLocation("density");
	location_gradient = ShaderProgram::getUniformLocation("gradient");
	location_numberOfRows = ShaderProgram::getUniformLocation("numberOfRows");
	location_cameraPos = ShaderProgram::getUniformLocation("cameraPos");
	location_offset = ShaderProgram::getUniformLocation("offset");
	location_displacementFactor = ShaderProgram::getUniformLocation("displacementFactor");
	location_plane = ShaderProgram::getUniformLocation("plane");
	location_textureSampler = ShaderProgram::getUniformLocation("textureSampler");
	location_displacementSampler = ShaderProgram::getUniformLocation("displacementSampler");
	
	std::stringstream stream;
	for (int i = 0; i < MAX_BONES; i++)
	{
		stream.str(std::string());
		stream << "boneMatrices[" << i << "]";
		location_bones[i] = ShaderProgram::getUniformLocation(stream.str().c_str());
	}

	for (int i = 0; i < MAX_LIGHTS; i++) {
		stream.str(std::string());
		stream << "lightPosition[" << i << "]";
		location_lightPosition[i] = ShaderProgram::getUniformLocation(stream.str().c_str());
		stream.str(std::string());
		stream << "lightColor[" << i << "]";
		location_lightColor[i] = ShaderProgram::getUniformLocation(stream.str().c_str());
		stream.str(std::string());
		stream << "attenuation[" << i << "]";
		location_attenuation[i] = ShaderProgram::getUniformLocation(stream.str().c_str());
	}
}
void EntityShader::bindAttributes()
{
	ShaderProgram::bindAttribute(0, "position");
	ShaderProgram::bindAttribute(1, "textureCoords");
	ShaderProgram::bindAttribute(2, "normal");
	ShaderProgram::bindAttribute(3, "boneIDs");
	ShaderProgram::bindAttribute(4, "boneWeights");
}
