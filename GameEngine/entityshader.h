#ifndef ENTITYSHADER_H
#define ENTITYSHADER_H
#define MAX_BONES 200
#include "shader.h"
class EntityShader : public ShaderProgram
{
public:
	EntityShader();
	~EntityShader();

	void loadDisplacement(GLfloat displacementFactor);
	void loadOffset(GLfloat x, GLfloat y);
	void loadFog(GLfloat gradient, GLfloat density);
	void loadSkyColor(glm::vec3 skyColor);
	void loadFakeLighting(GLboolean useFake);
	void loadShineVariables(GLfloat damper, GLfloat reflectivity);
	void loadViewMatrix(Camera* camera);
	void loadTransformationMatrix(glm::mat4 matrix);
	void loadProjectionMatrix(glm::mat4 projectionMatrix);
	void loadPlane(glm::vec4 plane);
	void loadLights();
	void loadTextures();
	void loadBones(glm::mat4* matrices, GLuint bones);
protected:
	void getAllUniformLocations();
	void bindAttributes();
private:
	GLuint location_transformationMatrix;
	GLuint location_projectionMatrix;
	GLuint location_viewMatrix;
	GLuint location_lightPosition[MAX_LIGHTS];
	GLuint location_lightColor[MAX_LIGHTS];
	GLuint location_reflectivity;
	GLuint location_shineDamper;
	GLuint location_useFakeLighting;
	GLuint location_skyColor;
	GLuint location_density;
	GLuint location_gradient;
	GLuint location_numberOfRows;
	GLuint location_offset;
	GLuint location_attenuation[MAX_LIGHTS];
	GLuint location_cameraPos;
	GLuint location_displacementFactor;
	GLuint location_plane;
	GLuint location_textureSampler;
	GLuint location_displacementSampler;
	GLuint location_bones[MAX_BONES];
};
#endif