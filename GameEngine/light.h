#ifndef LIGHT_H
#define LIGHT_H
#include <glm\vec3.hpp>
class Light
{
public:
	glm::vec3 position;
	glm::vec3 color;
	glm::vec3 attenuation;
	Light(glm::vec3 position, glm::vec3 color)
		:position{ position }, color{ color }, attenuation{ glm::vec3(1,0,0) } {}

	Light(glm::vec3 position, glm::vec3 color, glm::vec3 attenuation)
		:position{ position }, color{ color }, attenuation{ attenuation } {}
};
#endif /* !LIGHT_H */