#include "loader.h"

Loader::Loader(GLfloat* currentTime) {
	ilInit();
	glEnable(GL_TEXTURE_2D);
	this->currentTime = currentTime;
}

Loader::~Loader()
{
	std::list<GLuint>::iterator vaoB = vaos.begin();
	for (vaoB; vaoB != vaos.end(); vaoB++) {
		glDeleteVertexArrays(1, &*vaoB);
	}
	std::list<GLuint>::iterator vboB = vbos.begin();
	for (vboB; vboB != vbos.end(); vboB++) {
		glDeleteBuffers(1, &*vboB);
	}
	std::list<GLuint>::iterator texB = textures.begin();
	for (texB; texB != textures.end(); texB++) {
		glDeleteTextures(1, &*texB);
	}
}
RawModel * Loader::loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat normals[], GLuint normalsLength, GLuint * boneIDs, GLuint boneIDLength, GLfloat * boneWeights, GLuint boneWeightsLength, GLuint indices[], GLuint indicesLength)
{
	GLuint vaoID = createVAO();
	bindIndicesBuffer(indices, indicesLength);
	storeDataInAttributeList(0, positions, positionsLength, 3);
	storeDataInAttributeList(1, texCoords, texCoordsLength, 2);
	storeDataInAttributeList(2, normals, normalsLength, 3);
	storeDataInAttributeList(3, boneIDs, boneIDLength, 4);
	storeDataInAttributeList(4, boneWeights, boneWeightsLength, 4);
	unbindVAO();
	return new RawModel(vaoID, indicesLength / sizeof(GLuint));
}
RawModel * Loader::loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat normals[], GLuint normalsLength, GLuint * boneIDs, GLuint boneIDLength, GLfloat * boneWeights, GLuint boneWeightsLength, GLuint indices[], GLuint indicesLength, const char * name)
{
	GLuint vaoID = createVAO();
	bindIndicesBuffer(indices, indicesLength);
	storeDataInAttributeList(0, positions, positionsLength, 3);
	storeDataInAttributeList(1, texCoords, texCoordsLength, 2);
	storeDataInAttributeList(2, normals, normalsLength, 3);
	storeDataInAttributeList(3, boneIDs, boneIDLength, 4);
	storeDataInAttributeList(4, boneWeights, boneWeightsLength, 4);
	unbindVAO();
	return new RawModel(vaoID, indicesLength / sizeof(GLuint), name);
}
RawModel* Loader::loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat normals[], GLuint normalsLength, GLuint indices[], GLuint indicesLength)
{
	GLuint vaoID = createVAO();
	bindIndicesBuffer(indices, indicesLength);
	storeDataInAttributeList(0, positions, positionsLength, 3);
	storeDataInAttributeList(1, texCoords, texCoordsLength, 2);
	storeDataInAttributeList(2, normals, normalsLength, 3);
	unbindVAO();
	return new RawModel(vaoID, indicesLength/sizeof(GLuint));
}
RawModel* Loader::loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat normals[], GLuint normalsLength, GLuint indices[], GLuint indicesLength, const char* name)
{
	GLuint vaoID = createVAO();
	bindIndicesBuffer(indices, indicesLength);
	storeDataInAttributeList(0, positions, positionsLength, 3);
	storeDataInAttributeList(1, texCoords, texCoordsLength, 2);
	storeDataInAttributeList(2, normals, normalsLength, 3);
	unbindVAO();
	return new RawModel(vaoID, indicesLength/sizeof(GLuint), name);
}
RawModel * Loader::loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat vertexColors[], GLuint vertexColorLength, GLfloat normals[], GLuint normalsLength, GLuint indices[], GLuint indicesLength)
{
	GLuint vaoID = createVAO();
	bindIndicesBuffer(indices, indicesLength);
	storeDataInAttributeList(0, positions, positionsLength, 3);
	storeDataInAttributeList(1, texCoords, texCoordsLength, 2);
	storeDataInAttributeList(2, normals, normalsLength, 4);
	storeDataInAttributeList(3, positions, positionsLength, 3);
	unbindVAO();
	return new RawModel(vaoID, indicesLength / sizeof(GLuint));
}
RawModel * Loader::loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat vertexColors[], GLuint vertexColorLength, GLfloat normals[], GLuint normalsLength, GLuint indices[], GLuint indicesLength, const char * name)
{
	GLuint vaoID = createVAO();
	bindIndicesBuffer(indices, indicesLength);
	storeDataInAttributeList(0, positions, positionsLength, 3);
	storeDataInAttributeList(1, positions, positionsLength, 2);
	storeDataInAttributeList(2, positions, positionsLength, 4);
	storeDataInAttributeList(3, positions, positionsLength, 3);
	unbindVAO();
	return new RawModel(vaoID, indicesLength / sizeof(GLuint), name);
}
RawModel* Loader::loadToVAO(GLfloat positions[], GLuint positionsLength, GLint dimensions)
{
	GLuint vaoID = createVAO();
	storeDataInAttributeList(0, positions, positionsLength, dimensions);
	unbindVAO();
	return new RawModel(vaoID, positionsLength / sizeof(GLfloat) / dimensions);
}
RawModel* Loader::loadToVAO(GLfloat positions[], GLuint positionsLength, GLint dimensions, char* name)
{
	GLuint vaoID = createVAO();
	storeDataInAttributeList(0, positions, positionsLength, dimensions);
	unbindVAO();
	return new RawModel(vaoID, positionsLength / sizeof(GLfloat) / dimensions, name);
}
TextureData* Loader::decodeTextureFile(const char* fileName)
{
	unsigned int width = 0;
	unsigned int height = 0;
	ILuint image;
	ilGenImages(1, &image);
	ilBindImage(image);
	ilLoadImage(fileName);
	ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
	width = ilGetInteger(IL_IMAGE_WIDTH);
	height = ilGetInteger(IL_IMAGE_HEIGHT);
	GLubyte* data = ilGetData();
	ILenum error;
	if ((error = ilGetError()) != IL_NO_ERROR)
	{
		std::cout << error << std::endl;
		return nullptr;
	}
	return new TextureData(data, width*height*4, width, height);
}
GLuint Loader::loadCubeMap(const char * textureFiles[])
{
	GLuint texID;
	glGenTextures(1, &texID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texID);
	for (int i = 0; i < 6; i++) {
		TextureData* data = decodeTextureFile(textureFiles[i]);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, data->getWidth(), data->getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, data->getBuffer());
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	textures.push_back(texID);
	return texID;
}
GLuint Loader::loadTexture(const char * fileName)
{
	if (std::string(fileName).empty())
	{
		return loadTexture({ 0 }, 1);
	}
	TextureData* data = decodeTextureFile(fileName);
	if (data == nullptr)
	{
		return 0;
	}
	GLuint texID;
	glGenTextures(1, &texID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, data->getWidth(), data->getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, data->getBuffer());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -0.4f);
	glBindTexture(GL_TEXTURE_2D, 0);
	textures.push_back(texID);
	return texID;
}
GLuint Loader::loadTexture(GLubyte pixels[], int width)
{
	GLuint textureID;
	glGenTextures(1, &textureID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, width, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -0.4f);
	textures.push_back(textureID);
	return textureID;
}
void sort(std::map<Bone*, std::string>::iterator begin, std::map<Bone*, std::string>::iterator end)
{

}
TexturedModel * Loader::loadMesh(const char * fileName)
{

	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(fileName, aiProcess_FlipUVs | aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_GenUVCoords);
	std::cout << importer.GetErrorString() << std::endl;
	RawModel** models = (RawModel**)malloc(sizeof(RawModel*)*scene->mNumMeshes);
	ModelTexture** textures = (ModelTexture**)malloc(sizeof(ModelTexture*)*scene->mNumMaterials);
	aiNode* rootNode = scene->mRootNode;
	std::map<std::string, Bone*> skeleton;
	Bone* root = new Bone();
	aiMatrix4x4 temp = rootNode->mTransformation;
	root->relativeTransform = glm::mat4(temp.a1, temp.a2, temp.a3, temp.a4,
		temp.b1, temp.b2, temp.b3, temp.b4,
		temp.c1, temp.c2, temp.c3, temp.c4,
		temp.d1, temp.d2, temp.d3, temp.d4);
	root->parent = root;
	skeleton[rootNode->mName.C_Str()] = root;

	loadBones(skeleton, rootNode);
	std::map<Bone*, std::string> reverseSkeleton;
	for (std::map<std::string, Bone*>::iterator it = skeleton.begin(); it != skeleton.end(); it++)
	{
		it->second->bindPose = (it->second->absoluteTransform);
		it->second->invBindPose = glm::inverse(it->second->bindPose);
	}
	for (std::map<std::string, Bone*>::iterator it = skeleton.begin(); it != skeleton.end(); it++) {
		Bone* b = it->second;
		if (b->parent == b)
		{
			b->absoluteTransform = b->relativeTransform;
		}
		else {
			b->absoluteTransform = b->parent->absoluteTransform * b->relativeTransform;
		}
		b->globalTransform = b->absoluteTransform * b->invBindPose;
	}
	for (size_t i = 0; i < scene->mNumMaterials; i++)
	{
		ModelTexture* temp = new ModelTexture();
		aiMaterial* material = scene->mMaterials[i];
		aiString string;

		material->Get(AI_MATKEY_TEXTURE_AMBIENT(0), string);
		std::string tempStr = std::string(string.C_Str());
		std::stringstream ss;
		if (tempStr != "") {
			tempStr = tempStr.substr(tempStr.find_last_of("/") + 1);
			ss << RES_PATH << tempStr;
		}
		temp->ambientID = loadTexture(ss.str().c_str());

		material->Get(AI_MATKEY_TEXTURE_DIFFUSE(0), string);
		tempStr = std::string(string.C_Str());
		ss.str(std::string());
		if (tempStr != "") {
			tempStr = tempStr.substr(tempStr.find_last_of("\\") + 1);
			ss << RES_PATH << tempStr;
		}
		temp->diffuseID = loadTexture(ss.str().c_str());

		material->Get(AI_MATKEY_TEXTURE_EMISSIVE(0), string);
		tempStr = std::string(string.C_Str());
		ss.str(std::string());
		if (tempStr != "") {
			tempStr = tempStr.substr(tempStr.find_last_of("\\") + 1);
			ss << RES_PATH << tempStr;
		}
		temp->emissiveID = loadTexture(ss.str().c_str());

		material->Get(AI_MATKEY_TEXTURE_DISPLACEMENT(0), string);
		tempStr = std::string(string.C_Str());
		ss.str(std::string());
		if (tempStr != "") {
			tempStr = tempStr.substr(tempStr.find_last_of("\\") + 1);
			ss << RES_PATH << tempStr;
		}
		temp->displacementID = loadTexture(ss.str().c_str());

		material->Get(AI_MATKEY_TEXTURE_SPECULAR(0), string);
		tempStr = std::string(string.C_Str());
		ss.str(std::string());
		if (tempStr != "") {
			tempStr = tempStr.substr(tempStr.find_last_of("\\") + 1);
			ss << RES_PATH << tempStr;
		}
		temp->specularID = loadTexture(ss.str().c_str());

		material->Get(AI_MATKEY_SHININESS, temp->shineDamper);

		material->Get(AI_MATKEY_REFLECTIVITY, temp->reflectivity);
		textures[i] = temp;
	}
	for (size_t i = 0; i < scene->mNumMeshes; i++)
	{
		RawModel* temp;
		aiMesh* mesh = scene->mMeshes[i];
		GLfloat* vertices = (GLfloat*)malloc(sizeof(GLfloat)*mesh->mNumVertices * 3);
		GLfloat* texCoords = (GLfloat*)malloc(sizeof(GLfloat)*mesh->mNumVertices * 2);
		GLfloat* normals = (GLfloat*)malloc(sizeof(GLfloat)*mesh->mNumVertices * 3);
		GLuint* boneIDs = (GLuint*)malloc(sizeof(GLuint)*mesh->mNumVertices * 4);
		GLfloat* boneWeights = (GLfloat*)malloc(sizeof(GLfloat)*mesh->mNumVertices * 4);
		GLint* tops = (GLint*)malloc(sizeof(GLint)*mesh->mNumVertices);
		for (int i = 0; i < mesh->mNumVertices; i++)
		{
			tops[i] = 0;
		}
		GLuint* indices = (GLuint*)malloc(sizeof(GLuint)*mesh->mNumFaces * 3);
		std::cout << "Faces " << mesh->mNumFaces << " vertices " << mesh->mNumVertices << " Bones " << mesh->mNumBones << std::endl;
		for (size_t j = 0; j < mesh->mNumVertices; j++)
		{
			vertices[j * 3 + 0] = mesh->mVertices[j].x;
			vertices[j * 3 + 1] = mesh->mVertices[j].y;
			vertices[j * 3 + 2] = mesh->mVertices[j].z;
			texCoords[j * 2 + 0] = mesh->mTextureCoords[0][j].x;
			texCoords[j * 2 + 1] = mesh->mTextureCoords[0][j].y;
			normals[j * 3 + 0] = mesh->mNormals[j].x;
			normals[j * 3 + 1] = mesh->mNormals[j].y;
			normals[j * 3 + 2] = mesh->mNormals[j].z;
		}
		for (size_t j = 0; j < mesh->mNumFaces; j++)
		{
			aiFace face = mesh->mFaces[j];
			indices[j * 3 + 0] = face.mIndices[0];
			indices[j * 3 + 1] = face.mIndices[1];
			indices[j * 3 + 2] = face.mIndices[2];
		}
		for (size_t j = 0; j < mesh->mNumBones; j++)
		{
			aiBone* bone = mesh->mBones[j];
			for (size_t k = 0; k < bone->mNumWeights; k++)
			{
				aiVertexWeight weight = bone->mWeights[k];
				if (tops[weight.mVertexId] == 4)
					continue;
				boneIDs[weight.mVertexId * 4 + tops[weight.mVertexId]] = skeleton[bone->mName.C_Str()]->id;//NodeID mit dem Namen des bone finden
				boneWeights[weight.mVertexId * 4 + tops[weight.mVertexId]] = weight.mWeight;
				tops[weight.mVertexId]++;
			}
		}

		temp = loadToVAO(vertices, sizeof(GLfloat)*mesh->mNumVertices * 3, texCoords, sizeof(GLfloat)*mesh->mNumVertices * 2,
			normals, sizeof(GLfloat)*mesh->mNumVertices * 3, boneIDs, sizeof(GLuint)*mesh->mNumVertices * 4,
			boneWeights, sizeof(GLfloat)*mesh->mNumVertices * 4,
			indices, sizeof(GLuint)*mesh->mNumFaces * 3);
		temp->setMaterialIndex(mesh->mMaterialIndex);
		models[i] = temp;
	}
	std::map<std::string, GLuint> animations;
	for (std::map<std::string, Bone*>::iterator it = skeleton.begin(); it != skeleton.end(); it++) {
		Bone* b = it->second;
		b->positionMaps.reserve(scene->mNumAnimations);
		b->positionMaps.resize(scene->mNumAnimations);
		b->rotationMaps.reserve(scene->mNumAnimations);
		b->rotationMaps.resize(scene->mNumAnimations);
		b->scaleMaps.reserve(scene->mNumAnimations);
		b->scaleMaps.resize(scene->mNumAnimations);
	}
	for (size_t i = 0; i < scene->mNumAnimations; i++)
	{
		aiAnimation* animation = scene->mAnimations[i];
		std::string name = animation->mName.data;
		animations[name] = i;
		for (int j = 0; j < animation->mNumChannels; j++)
		{
			aiNodeAnim* nodeAnim = animation->mChannels[j];
			std::map<std::string, Bone*>::iterator it = skeleton.find(nodeAnim->mNodeName.data);

			Bone* b = it->second;
			for (int k = 0; k < nodeAnim->mNumPositionKeys; k++)
			{
				aiVectorKey key = nodeAnim->mPositionKeys[k];
				b->positionMaps[i][key.mTime] = glm::translate(glm::mat4(), glm::vec3(key.mValue.x, key.mValue.y, key.mValue.z));
			}
			for (int k = 0; k < nodeAnim->mNumRotationKeys; k++)
			{
				aiQuatKey key = nodeAnim->mRotationKeys[k];
				b->rotationMaps[i][key.mTime] = glm::rotate(glm::mat4(), key.mValue.w, glm::vec3(key.mValue.x, key.mValue.y, key.mValue.z));
			}
			for (int k = 0; k < nodeAnim->mNumScalingKeys; k++)
			{
				aiVectorKey key = nodeAnim->mScalingKeys[k];
				b->scaleMaps[i][key.mTime] = glm::scale(glm::mat4(), glm::vec3(key.mValue.x, key.mValue.y, key.mValue.z));
			}
		}
	}
	std::vector<Bone*> bones;
	for (auto& b : skeleton)
	{
		bones.push_back(b.second);
	}
	std::sort(bones.begin(), bones.end(), less_than_key());
	TexturedModel* texModel = new TexturedModel(models, scene->mNumMeshes, textures, scene->mNumMeshes, currentTime);
	texModel->setSkeleton(bones);
	texModel->setAnimations(scene->mAnimations, scene->mNumAnimations);
	texModel->animations = animations;
	return texModel;
}

GLuint Loader::createVAO()
{
	GLuint vaoID;
	glGenVertexArrays(1, &vaoID);
	vaos.push_back(vaoID);
	glBindVertexArray(vaoID);
	return vaoID;
}
void Loader::loadBones(std::map<std::string, Bone*>& skeleton, aiNode * node)
{

	for (int i = 0; i < node->mNumChildren; i++)
	{
		aiNode* child = node->mChildren[i];
		Bone* b = new Bone();
		b->parent = skeleton[std::string(node->mName.C_Str())];
		aiMatrix4x4 temp = child->mTransformation;
		b->relativeTransform = glm::mat4(temp.a1, temp.a2, temp.a3, temp.a4,
			temp.b1, temp.b2, temp.b3, temp.b4,
			temp.c1, temp.c2, temp.c3, temp.c4,
			temp.d1, temp.d2, temp.d3, temp.d4);
		b->id = skeleton.size();
		skeleton[child->mName.C_Str()] = b;
		loadBones(skeleton, child);
	}
}
void Loader::storeDataInAttributeList(GLuint attributeNumber, GLfloat data[], GLuint length, GLint dimensions)
{
	GLuint vboID;
	glGenBuffers(1, &vboID);
	vbos.push_back(vboID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	glBufferData(GL_ARRAY_BUFFER, length, data, GL_STATIC_DRAW);
	glVertexAttribPointer(attributeNumber, dimensions, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
void Loader::storeDataInAttributeList(GLuint attributeNumber, GLuint data[], GLuint length, GLint dimensions)
{
	GLuint vboID;
	glGenBuffers(1, &vboID);
	vbos.push_back(vboID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	glBufferData(GL_ARRAY_BUFFER, length, data, GL_STATIC_DRAW);
	glVertexAttribIPointer(attributeNumber, dimensions, GL_UNSIGNED_INT, 0, (void*)0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
void Loader::unbindVAO()
{
	glBindVertexArray(0);
}

void Loader::bindIndicesBuffer(GLuint indices[], GLuint indicesLength)
{
	GLuint vboID;
	glGenBuffers(1, &vboID);
	vbos.push_back(vboID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesLength, indices, GL_STATIC_DRAW);
}
