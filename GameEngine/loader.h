#ifndef LOADER_H
#define LOADER_H

#include <IL\il.h>
#include <list>
#include <vector>
#include <map>
#include <glm\vec3.hpp>
#include <glm\vec2.hpp>
#include <glm\matrix.hpp>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <assimp\Importer.hpp>
#include <assimp\postprocess.h>
#include <assimp\scene.h>
#include <GL\glew.h>
#include "rawmodel.h"
#include "texmodel.h"
#include "data.h"


#define NUM_BONES_PER_VERTEX 4
#define INVALID_MATERIAL 0xFFFFFFFF
struct less_than_key
{
	inline bool operator() (const Bone* struct1, const Bone* struct2)
	{
		return (struct1->id < struct2->id);
	}
};
class Vertex
{
private:
	static const GLint NO_INDEX = -1;
public:	
	glm::vec3 position;
	GLint textureIndex = NO_INDEX;
	GLint normalIndex = NO_INDEX;
	Vertex* duplicateVertex;
	GLint index;
	float length;
	Vertex(GLint index, glm::vec3 position)
		:index{ index }, position{ position }, length{ (float)position.length() }, duplicateVertex{ NULL } {
	}
	~Vertex(){
		delete duplicateVertex;
	}
	bool isSet() { return textureIndex != NO_INDEX&&normalIndex != NO_INDEX; }
	bool hasSamerTextureAndNormal(GLint otherTex, GLint otherNormal) { return otherTex == textureIndex&&normalIndex == otherNormal; }
};

class ModelData
{
public:
	GLfloat* vertices;
	GLfloat* textureCoords;
	GLfloat* normals;
	GLuint* indices;
	GLuint vertexCount;
	GLuint indicesCount;
	float furthestPoint;
	ModelData(GLfloat* vertices, GLfloat* texCoords, GLfloat* normals, GLuint vertexCount, GLuint* indices, GLuint indicesCount, float furthestPoint)
		:vertices{ vertices }, textureCoords{ texCoords }, normals{ normals }, vertexCount{ vertexCount }, indices {
		indices
	}, indicesCount{ indicesCount }, furthestPoint{ furthestPoint } {
	}
	~ModelData() {
		delete vertices;
		delete textureCoords;
		delete normals;
		delete indices;
	}
};
class TextureData
{

private:
	GLint width;
	GLint height;
	GLubyte* buffer;
	GLuint bufferLength;

public :
	TextureData(GLubyte* buffer, GLuint bufferLength, GLint width, GLint height) 
		:buffer{ buffer }, bufferLength{ bufferLength }, width{ width }, height{ height } {
	}
	~TextureData() {
		delete buffer;
	}

public:
	GLint getWidth() {
		return width;
	}

	GLint getHeight() {
		return height;
	}

	GLubyte* getBuffer() {
		return buffer;
	}
	GLuint getBufferLength() {
		return bufferLength;
	}
};

class Loader 
{
public:
	Loader(GLfloat* currentTime);
	~Loader();
	RawModel* loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat normals[], GLuint normalsLength, GLuint* boneIDs, GLuint boneIDLength, GLfloat* boneWeights, GLuint boneWeightsLength, GLuint indices[], GLuint indicesLength);
	RawModel* loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat normals[], GLuint normalsLength, GLuint* boneIDs, GLuint boneIDLength, GLfloat* boneWeights, GLuint boneWeightsLength, GLuint indices[], GLuint indicesLength, const char* name);
	RawModel* loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat normals[], GLuint normalsLength, GLuint indices[], GLuint indicesLength);
	RawModel* loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat normals[], GLuint normalsLength, GLuint indices[], GLuint indicesLength, const char* name);
	RawModel* loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat vertexColors[], GLuint vertexColorLength, GLfloat normals[], GLuint normalsLength, GLuint indices[], GLuint indicesLength);
	RawModel* loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat vertexColors[], GLuint vertexColorLength, GLfloat normals[], GLuint normalsLength, GLuint indices[], GLuint indicesLength, const char* name);
	RawModel* loadToVAO(GLfloat positions[], GLuint positionsLength, GLint dimensions);
	RawModel* loadToVAO(GLfloat positions[], GLuint positionsLength, GLint dimensions, char* name);
	GLuint loadCubeMap(const char* textureFiles[]);
	GLuint loadTexture(const char* fileName);
	GLuint loadTexture(GLubyte pixels[], int width);
	TexturedModel* loadMesh(const char* fileName);
private:
	std::list<GLuint> vaos;
	std::list<GLuint> vbos;
	std::list<GLuint> textures;
	TextureData* decodeTextureFile(const char* fileName);
	GLuint createVAO();
	void loadBones(std::map<std::string, Bone*>& skeleton, aiNode* node);
	void storeDataInAttributeList(GLuint attributeNumber, GLfloat data[], GLuint lenght, GLint dimensions);
	void storeDataInAttributeList(GLuint attributeNumber, GLuint data[], GLuint lenght, GLint dimensions);
	void unbindVAO();
	void bindIndicesBuffer(GLuint indices[], GLuint indicesLength);
	GLfloat* currentTime;
};

#endif /* !LOADER_H */
