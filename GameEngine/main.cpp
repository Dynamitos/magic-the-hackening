#include <GL\glew.h>
#include "renderer.h"
GLfloat vertices[] = {-0.3, 0.4, 0,
					-0.7, -0.1, 0,
					0.5, -0.2, 0, 
					0.5, 0.5, 0};
GLfloat texCoords[] = {0, 0, 0, 1, 1, 0, 1, 1};
GLfloat normals[] = { 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0 };
GLuint indices[] = { 0,1,3,
				 3,1,2 };
int main() {
	bool running = true;
	createDisplay();
	//75MB
	Loader* loader = new Loader(&currentTime);
	Data::loadResources(loader);
	//133MB
	//TexturedModel* model = new TexturedModel(&rawModel, 1, &texture, 1);
	TexturedModel* model = loader->loadMesh("C:\\Program Files\\Java\\res\\astroBoy_walk_Maya.xml");
	//190MB
	Entity* entity = new Entity(model, glm::vec3(1, 0, 5), 0, 0, 0, 1);
	std::multimap<TexturedModel*, Entity*> entities;
	entities.insert({ model, entity });
	Status* status = NULL;
	Player* player = new Player(NULL, glm::vec3(10, 0, 10), 0, 0, 0, 1, NULL);
	entities.insert({ model, player });

	Camera* camera = new Camera(player, deltaX, deltaY, rotation);
	MasterRenderer* renderer = new MasterRenderer(loader, &delta);
	//254MB
	while(running) {
		player->move(keys, getFrameTimeSeconds());
		camera->move();
		renderer->render(camera, entities);
		updateDisplay();
		if (keys[CLOSE_KEY])
			running = false;
	}
	closeDisplay();
}