#ifndef MATHS_H
#define MATHS_H
#include <glm\vec3.hpp>
#include <glm\vec2.hpp>
#include <glm\mat4x4.hpp>
#include <glm\gtx\transform.hpp>

#define PI 3.14159
#define toRadians(x) x*PI/180

static float barryCentric(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec2 pos) {
	float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
	float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
	float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
	float l3 = 1.0f - l1 - l2;
	return l1 * p1.y + l2 * p2.y + l3 * p3.y;
}

static glm::mat4 createTransformationMatrix(glm::vec3 translation, float rx, float ry, float rz, float scale) {
	glm::mat4 matrix = glm::mat4();
	matrix = glm::translate(translation);
	matrix = glm::rotate(matrix, (float)(toRadians(rx)), glm::vec3(1, 0, 0));
	matrix = glm::rotate(matrix, (float)(toRadians(ry)), glm::vec3(0, 1, 0));
	matrix = glm::rotate(matrix, (float)(toRadians(rz)), glm::vec3(0, 0, 1));
	return matrix;
}

static glm::mat4 createViewMatrix(Camera* camera) {
	glm::mat4 viewMatrix = glm::mat4(1);
	viewMatrix = glm::rotate(viewMatrix, (float)(toRadians(camera->pitch)), glm::vec3(1, 0, 0));
	viewMatrix = glm::rotate(viewMatrix, (float)(toRadians(camera->yaw)), glm::vec3(0, 1, 0));
	glm::vec3 cameraPos = camera->position;
	glm::vec3 negativeCameraPos = glm::vec3(-cameraPos.x, -cameraPos.y, -cameraPos.z);
	viewMatrix = glm::translate(viewMatrix, negativeCameraPos);
	return viewMatrix;
}

static glm::mat4 createTransformationMatrix(glm::vec2 translation, glm::vec2 scale) {
	glm::mat4 matrix = glm::mat4(1);
	matrix = glm::translate(matrix, glm::vec3(translation, 0));
	matrix = glm::scale(matrix, glm::vec3(scale.x, scale.y, 1));

	return matrix;
}
#endif /* !MATHS_H */