#include "model.h"


#define POSITION_LOCATION    0
#define TEX_COORD_LOCATION   1
#define NORMAL_LOCATION      2
#define BONE_ID_LOCATION     3
#define BONE_WEIGHT_LOCATION 4

void AnimatedModel::VertexBoneData::addBoneData(GLuint BoneID, float Weight)
{
	for (GLuint i = 0; i < 4; i++) {
		if (weights[i] == 0.0) {
			ids[i] = BoneID;
			weights[i] = Weight;
			return;
		}
	}
}

AnimatedModel::AnimatedModel(Loader* loader)
	:loader{ loader } {
	m_VAO = 0;
	for (int i = 0; i < 5; i++) {
		m_Buffers[i] = 0;
	}
	m_numBones = 0;
	m_pScene = NULL;
}


AnimatedModel::~AnimatedModel()
{
	clear();
}


void AnimatedModel::clear()
{
	if (m_Buffers[0] != 0) {
		glDeleteBuffers(5, m_Buffers);
	}

	if (m_VAO != 0) {
		glDeleteVertexArrays(1, &m_VAO);
		m_VAO = 0;
	}
}


TexturedModel* AnimatedModel::loadMesh(const char* fileName)
{
	TexturedModel* model;

	bool Ret = false;

	m_pScene = m_Importer.ReadFile(fileName, aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs);

	if (m_pScene) {
		aiMatrix4x4 temp = m_pScene->mRootNode->mTransformation;
		m_globalInverseTransform = glm::mat4(temp.a1, temp.a2, temp.a3, temp.a4, 
										temp.b1, temp.b2, temp.b3, temp.b4, 
										temp.c1, temp.c2, temp.c3, temp.c4,
										temp.d1, temp.d2, temp.d3, temp.d4);
		inverse(m_globalInverseTransform);
		initFromScene(m_pScene, fileName, model);
	}
	else {
		printf("Error parsing '%s': '%s'\n", fileName, m_Importer.GetErrorString());
	}
}


bool AnimatedModel::initFromScene(const aiScene* pScene, const char* fileName, TexturedModel* model)
{
	m_entries.resize(pScene->mNumMeshes);
	m_textures.resize(pScene->mNumMaterials);

	vector<glm::vec3> Positions;
	vector<glm::vec3> Normals;
	vector<glm::vec2> TexCoords;
	vector<VertexBoneData> Bones;
	vector<GLuint> Indices;

	RawModel** rawModels = (RawModel**)malloc(sizeof(RawModel**)*m_pScene->mNumMeshes);
	ModelTexture** textures = (ModelTexture**)malloc(sizeof(ModelTexture**)*m_pScene->mNumMaterials);

	GLuint NumVertices = 0;
	GLuint NumIndices = 0;

	// Count the number of vertices and indices
	for (GLuint i = 0; i < m_entries.size(); i++) {
		m_entries[i].MaterialIndex = pScene->mMeshes[i]->mMaterialIndex;
		m_entries[i].NumIndices = pScene->mMeshes[i]->mNumFaces * 3;
		m_entries[i].BaseVertex = NumVertices;
		m_entries[i].BaseIndex = NumIndices;

		NumVertices += pScene->mMeshes[i]->mNumVertices;
		NumIndices += m_entries[i].NumIndices;
	}

	// Initialize the meshes in the scene one by one
	for (GLuint i = 0; i < m_entries.size(); i++) {
		const aiMesh* paiMesh = pScene->mMeshes[i];
		initMesh(i, paiMesh, Positions, Normals, TexCoords, Bones, Indices, rawModels[i]);
	}

	if (!initMaterials(pScene, fileName, textures)) {
		return false;
	}
	model = new TexturedModel(rawModels, m_pScene->mNumMeshes, textures, m_pScene->mNumMaterials);
	return true;
}

void AnimatedModel::initMesh(GLuint MeshIndex,
	const aiMesh* paiMesh,
	vector<glm::vec3>& Positions,
	vector<glm::vec3>& Normals,
	vector<glm::vec2>& TexCoords,
	vector<VertexBoneData>& Bones,
	vector<GLuint>& Indices,
	RawModel* model)
{
	const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);

	// Populate the vertex attribute vectors
	for (GLuint i = 0; i < paiMesh->mNumVertices; i++) {
		const aiVector3D* pPos = &(paiMesh->mVertices[i]);
		const aiVector3D* pNormal = &(paiMesh->mNormals[i]);
		const aiVector3D* pTexCoord = paiMesh->HasTextureCoords(0) ? &(paiMesh->mTextureCoords[0][i]) : &Zero3D;

		Positions.push_back(glm::vec3(pPos->x, pPos->y, pPos->z));
		Normals.push_back(glm::vec3(pNormal->x, pNormal->y, pNormal->z));
		TexCoords.push_back(glm::vec2(pTexCoord->x, pTexCoord->y));
	}
	GLfloat* positions = (GLfloat*)malloc(sizeof(GLfloat) * 3 * Positions.size());
	GLfloat* texCoords = (GLfloat*)malloc(sizeof(GLfloat) * 2 * TexCoords.size());
	GLfloat* normals = (GLfloat*)malloc(sizeof(GLfloat) * 3 * Normals.size());
	for (int i = 0; i < Positions.size(); i++)
	{
		positions[i * 3 + 0] = Positions[i].x;
		positions[i * 3 + 1] = Positions[i].y;
		positions[i * 3 + 2] = Positions[i].z;
		texCoords[i * 2 + 0] = TexCoords[i].x;
		texCoords[i * 2 + 1] = TexCoords[i].y;
		normals[i * 3 + 0] = Normals[i].x;
		normals[i * 3 + 1] = Normals[i].y;
		normals[i * 3 + 2] = Normals[i].z;
	}
	loadBones(MeshIndex, paiMesh, Bones);
	GLuint* boneIDs = (GLuint*)malloc(sizeof(GLuint) * 4 * Bones.size());
	GLfloat* boneWeights = (GLfloat*)malloc(sizeof(GLfloat) * 4 * Bones.size());
	for (int i = 0; i < Bones.size(); i++)
	{
		boneIDs[i * 4 + 0] = Bones[i].ids[0];
		boneIDs[i * 4 + 1] = Bones[i].ids[1];
		boneIDs[i * 4 + 2] = Bones[i].ids[2];
		boneIDs[i * 4 + 3] = Bones[i].ids[3];
		boneWeights[i * 4 + 0] = Bones[i].weights[0];
		boneWeights[i * 4 + 1] = Bones[i].weights[1];
		boneWeights[i * 4 + 2] = Bones[i].weights[2];
		boneWeights[i * 4 + 3] = Bones[i].weights[3];
	}
	// Populate the index buffer
	for (GLuint i = 0; i < paiMesh->mNumFaces; i++) {
		const aiFace& Face = paiMesh->mFaces[i];
		assert(Face.mNumIndices == 3);
		Indices.push_back(Face.mIndices[0]);
		Indices.push_back(Face.mIndices[1]);
		Indices.push_back(Face.mIndices[2]);
	}
	GLuint* indices = &Indices[0];
	model = loader->loadToVAO(positions, sizeof(GLfloat) * 3 * Positions.size(),
		texCoords, sizeof(GLfloat) * 2 * TexCoords.size(), normals,
		sizeof(GLfloat) * 3 * Normals.size(), boneIDs, sizeof(GLuint) * 4 * Bones.size(), boneWeights,
		sizeof(GLfloat) * 4 * Bones.size(), indices, sizeof(GLuint)*Indices.size(), paiMesh->mName.C_Str());
}


void AnimatedModel::loadBones(GLuint MeshIndex, const aiMesh* pMesh, vector<VertexBoneData>& Bones)
{
	for (GLuint i = 0; i < pMesh->mNumBones; i++) {
		GLuint BoneIndex = 0;
		string BoneName(pMesh->mBones[i]->mName.data);

		if (m_boneMapping.find(BoneName) == m_boneMapping.end()) {
			// Allocate an index for a new bone
			BoneIndex = m_numBones;
			m_numBones++;
			BoneInfo bi;
			m_boneInfo.push_back(bi);
			aiMatrix4x4 temp = pMesh->mBones[i]->mOffsetMatrix;
			m_boneInfo[BoneIndex].boneOffset = glm::mat4(temp.a1, temp.a2, temp.a3, temp.a4,
				temp.b1, temp.b2, temp.b3, temp.b4,
				temp.c1, temp.c2, temp.c3, temp.c4,
				temp.d1, temp.d2, temp.d3, temp.d4);
			m_boneMapping[BoneName] = BoneIndex;
		}
		else {
			BoneIndex = m_boneMapping[BoneName];
		}

		for (GLuint j = 0; j < pMesh->mBones[i]->mNumWeights; j++) {
			GLuint VertexID = m_entries[MeshIndex].BaseVertex + pMesh->mBones[i]->mWeights[j].mVertexId;
			float Weight = pMesh->mBones[i]->mWeights[j].mWeight;
			Bones[VertexID].addBoneData(BoneIndex, Weight);
		}
	}
}


bool AnimatedModel::initMaterials(const aiScene* pScene, const string& Filename, ModelTexture** textures)
{
	bool Ret = true;

	// Initialize the materials
	for (GLuint i = 0; i < pScene->mNumMaterials; i++) {
		const aiMaterial* pMaterial = pScene->mMaterials[i];


		if (pMaterial->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
			aiString Path;

			if (pMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &Path, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS) {
				string name = string(Path.C_Str());
				name = name.substr(2);
				stringstream stream;
				stream << RES_PATH << name;
				string FullPath = stream.str();
				textures[i] = new ModelTexture(loader->loadTexture(FullPath.c_str()));
			}
		}
	}

	return Ret;
}


void AnimatedModel::Render()
{
	glBindVertexArray(m_VAO);

	for (GLuint i = 0; i < m_entries.size(); i++) {
		const GLuint MaterialIndex = m_entries[i].MaterialIndex;

		assert(MaterialIndex < m_textures.size());
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_textures[i].textureID);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, m_textures[i].displacementTextureID);

		glDrawElementsBaseVertex(GL_TRIANGLES,
			m_entries[i].NumIndices,
			GL_UNSIGNED_INT,
			(void*)(sizeof(GLuint) * m_entries[i].BaseIndex),
			m_entries[i].BaseVertex);
	}

	glBindTexture(GL_TEXTURE_2D, 0);
	// Make sure the VAO is not changed from the outside    
	glBindVertexArray(0);
}


GLuint AnimatedModel::findPosition(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	for (GLuint i = 0; i < pNodeAnim->mNumPositionKeys - 1; i++) {
		if (AnimationTime < (float)pNodeAnim->mPositionKeys[i + 1].mTime) {
			return i;
		}
	}

	assert(0);

	return 0;
}


glm::mat4 initScaleTransform(glm::mat4 matrix, float ScaleX, float ScaleY, float ScaleZ)
{
	matrix[0][0] = ScaleX; matrix[0][1] = 0.0f;   matrix[0][2] = 0.0f;   matrix[0][3] = 0.0f;
	matrix[1][0] = 0.0f;   matrix[1][1] = ScaleY; matrix[1][2] = 0.0f;   matrix[1][3] = 0.0f;
	matrix[2][0] = 0.0f;   matrix[2][1] = 0.0f;   matrix[2][2] = ScaleZ; matrix[2][3] = 0.0f;
	matrix[3][0] = 0.0f;   matrix[3][1] = 0.0f;   matrix[3][2] = 0.0f;   matrix[3][3] = 1.0f;
	return matrix;
}

glm::mat4 initRotateTransform(glm::mat4 matrix, float RotateX, float RotateY, float RotateZ)
{
	glm::mat4 rx, ry, rz;

	const float x = RotateX*3.1415953 / 180;
	const float y = RotateY*3.1415953 / 180;
	const float z = RotateZ*3.1415953 / 180;

	rx[0][0] = 1.0f; rx[0][1] = 0.0f; rx[0][2] = 0.0f; rx[0][3] = 0.0f;
	rx[1][0] = 0.0f; rx[1][1] = cosf(x); rx[1][2] = -sinf(x); rx[1][3] = 0.0f;
	rx[2][0] = 0.0f; rx[2][1] = sinf(x); rx[2][2] = cosf(x); rx[2][3] = 0.0f;
	rx[3][0] = 0.0f; rx[3][1] = 0.0f; rx[3][2] = 0.0f; rx[3][3] = 1.0f;

	ry[0][0] = cosf(y); ry[0][1] = 0.0f; ry[0][2] = -sinf(y); ry[0][3] = 0.0f;
	ry[1][0] = 0.0f; ry[1][1] = 1.0f; ry[1][2] = 0.0f; ry[1][3] = 0.0f;
	ry[2][0] = sinf(y); ry[2][1] = 0.0f; ry[2][2] = cosf(y); ry[2][3] = 0.0f;
	ry[3][0] = 0.0f; ry[3][1] = 0.0f; ry[3][2] = 0.0f; ry[3][3] = 1.0f;

	rz[0][0] = cosf(z); rz[0][1] = -sinf(z); rz[0][2] = 0.0f; rz[0][3] = 0.0f;
	rz[1][0] = sinf(z); rz[1][1] = cosf(z); rz[1][2] = 0.0f; rz[1][3] = 0.0f;
	rz[2][0] = 0.0f; rz[2][1] = 0.0f; rz[2][2] = 1.0f; rz[2][3] = 0.0f;
	rz[3][0] = 0.0f; rz[3][1] = 0.0f; rz[3][2] = 0.0f; rz[3][3] = 1.0f;

	matrix = rx*ry*rz;

	return matrix;
}

glm::mat4 initTranslationTransform(glm::mat4 matrix, float x, float y, float z)
{
	matrix[0][0] = 1.0f; matrix[0][1] = 0.0f; matrix[0][2] = 0.0f; matrix[0][3] = x;
	matrix[1][0] = 0.0f; matrix[1][1] = 1.0f; matrix[1][2] = 0.0f; matrix[1][3] = y;
	matrix[2][0] = 0.0f; matrix[2][1] = 0.0f; matrix[2][2] = 1.0f; matrix[2][3] = z;
	matrix[3][0] = 0.0f; matrix[3][1] = 0.0f; matrix[3][2] = 0.0f; matrix[3][3] = 1.0f;
	return matrix;
}
GLuint AnimatedModel::findRotation(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumRotationKeys > 0);

	for (GLuint i = 0; i < pNodeAnim->mNumRotationKeys - 1; i++) {
		if (AnimationTime < (float)pNodeAnim->mRotationKeys[i + 1].mTime) {
			return i;
		}
	}

	assert(0);

	return 0;
}


GLuint AnimatedModel::findScaling(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumScalingKeys > 0);

	for (GLuint i = 0; i < pNodeAnim->mNumScalingKeys - 1; i++) {
		if (AnimationTime < (float)pNodeAnim->mScalingKeys[i + 1].mTime) {
			return i;
		}
	}

	assert(0);

	return 0;
}


void AnimatedModel::calcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumPositionKeys == 1) {
		Out = pNodeAnim->mPositionKeys[0].mValue;
		return;
	}

	GLuint PositionIndex = findPosition(AnimationTime, pNodeAnim);
	GLuint NextPositionIndex = (PositionIndex + 1);
	assert(NextPositionIndex < pNodeAnim->mNumPositionKeys);
	float DeltaTime = (float)(pNodeAnim->mPositionKeys[NextPositionIndex].mTime - pNodeAnim->mPositionKeys[PositionIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mPositionKeys[PositionIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiVector3D& Start = pNodeAnim->mPositionKeys[PositionIndex].mValue;
	const aiVector3D& End = pNodeAnim->mPositionKeys[NextPositionIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
}


void AnimatedModel::calcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	// we need at least two values to interpolate...
	if (pNodeAnim->mNumRotationKeys == 1) {
		Out = pNodeAnim->mRotationKeys[0].mValue;
		return;
	}

	GLuint RotationIndex = findRotation(AnimationTime, pNodeAnim);
	GLuint NextRotationIndex = (RotationIndex + 1);
	assert(NextRotationIndex < pNodeAnim->mNumRotationKeys);
	float DeltaTime = (float)(pNodeAnim->mRotationKeys[NextRotationIndex].mTime - pNodeAnim->mRotationKeys[RotationIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mRotationKeys[RotationIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiQuaternion& StartRotationQ = pNodeAnim->mRotationKeys[RotationIndex].mValue;
	const aiQuaternion& EndRotationQ = pNodeAnim->mRotationKeys[NextRotationIndex].mValue;
	aiQuaternion::Interpolate(Out, StartRotationQ, EndRotationQ, Factor);
	Out = Out.Normalize();
}


void AnimatedModel::calcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumScalingKeys == 1) {
		Out = pNodeAnim->mScalingKeys[0].mValue;
		return;
	}

	GLuint ScalingIndex = findScaling(AnimationTime, pNodeAnim);
	GLuint NextScalingIndex = (ScalingIndex + 1);
	assert(NextScalingIndex < pNodeAnim->mNumScalingKeys);
	float DeltaTime = (float)(pNodeAnim->mScalingKeys[NextScalingIndex].mTime - pNodeAnim->mScalingKeys[ScalingIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mScalingKeys[ScalingIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiVector3D& Start = pNodeAnim->mScalingKeys[ScalingIndex].mValue;
	const aiVector3D& End = pNodeAnim->mScalingKeys[NextScalingIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
}


void AnimatedModel::readNodeHeirarchy(float AnimationTime, const aiNode* pNode, glm::mat4& ParentTransform)
{
	string NodeName(pNode->mName.data);

	const aiAnimation* pAnimation = m_pScene->mAnimations[0];
	aiMatrix4x4 temp = pNode->mTransformation;
	glm::mat4 NodeTransformation(temp.a1, temp.a2, temp.a3, temp.a4,
		temp.b1, temp.b2, temp.b3, temp.b4,
		temp.c1, temp.c2, temp.c3, temp.c4,
		temp.d1, temp.d2, temp.d3, temp.d4);

	const aiNodeAnim* pNodeAnim = findNodeAnim(pAnimation, NodeName);

	if (pNodeAnim) {
		// Interpolate scaling and generate scaling transformation matrix
		aiVector3D Scaling;
		calcInterpolatedScaling(Scaling, AnimationTime, pNodeAnim);
		glm::mat4 ScalingM;
		ScalingM = initScaleTransform(ScalingM, Scaling.x, Scaling.y, Scaling.z);

		// Interpolate rotation and generate rotation transformation matrix
		aiQuaternion RotationQ;
		calcInterpolatedRotation(RotationQ, AnimationTime, pNodeAnim);
		aiMatrix3x3 temp = RotationQ.GetMatrix();
		glm::mat4 RotationM(temp.a1, temp.a2, temp.a3, 0,
			temp.b1, temp.b2, temp.b3, 0,
			temp.c1, temp.c2, temp.c3, 0,
			0, 0, 0, 0);

		// Interpolate translation and generate translation transformation matrix
		aiVector3D Translation;
		calcInterpolatedPosition(Translation, AnimationTime, pNodeAnim);
		glm::mat4 TranslationM;
		TranslationM = initTranslationTransform(TranslationM, Translation.x, Translation.y, Translation.z);

		// Combine the above transformations
		NodeTransformation = TranslationM * RotationM * ScalingM;
	}

	glm::mat4 GlobalTransformation = ParentTransform * NodeTransformation;

	if (m_boneMapping.find(NodeName) != m_boneMapping.end()) {
		GLuint BoneIndex = m_boneMapping[NodeName];
		m_boneInfo[BoneIndex].finalTransformation = m_globalInverseTransform * GlobalTransformation * m_boneInfo[BoneIndex].boneOffset;
	}

	for (GLuint i = 0; i < pNode->mNumChildren; i++) {
		readNodeHeirarchy(AnimationTime, pNode->mChildren[i], GlobalTransformation);
	}
}


void AnimatedModel::boneTransform(float TimeInSeconds, vector<glm::mat4>& Transforms)
{
	glm::mat4 Identity(1.0);

	float TicksPerSecond = (float)(m_pScene->mAnimations[0]->mTicksPerSecond != 0 ? m_pScene->mAnimations[0]->mTicksPerSecond : 25.0f);
	float TimeInTicks = TimeInSeconds * TicksPerSecond;
	float AnimationTime = fmod(TimeInTicks, (float)m_pScene->mAnimations[0]->mDuration);

	readNodeHeirarchy(AnimationTime, m_pScene->mRootNode, Identity);

	Transforms.resize(m_numBones);

	for (GLuint i = 0; i < m_numBones; i++) {
		Transforms[i] = m_boneInfo[i].finalTransformation;
	}
}


const aiNodeAnim* AnimatedModel::findNodeAnim(const aiAnimation* pAnimation, const string NodeName)
{
	for (GLuint i = 0; i < pAnimation->mNumChannels; i++) {
		const aiNodeAnim* pNodeAnim = pAnimation->mChannels[i];

		if (string(pNodeAnim->mNodeName.data) == NodeName) {
			return pNodeAnim;
		}
	}

	return NULL;
}
