#ifndef MODEL_H
#define MODEL_H
#include <GL\glew.h>
#include <glm\vec3.hpp>
#include <glm\mat4x4.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <assimp\Importer.hpp>
#include <assimp\postprocess.h>
#include <assimp\scene.h>
#include <assert.h>
#include <sstream>

#include <vector>
#include <map>

#include "loader.h"
#include "modeltex.h"
#include "rawmodel.h"
#include "texmodel.h"
#include "data.h"

using namespace std;

class AnimatedModel {
public:
	AnimatedModel(Loader* loader);

	~AnimatedModel();

	TexturedModel* loadMesh(const char* fileName);

	void Render();

	GLint NumBones() const
	{
		return m_numBones;
	}

	void boneTransform(float timeInSeconds, std::vector<glm::mat4>& transforms);
private:
#define NUM_BONES_PER_VERTEX 4
	struct BoneInfo 
	{
		glm::mat4 boneOffset;
		glm::mat4 finalTransformation;

		BoneInfo() {
			boneOffset = glm::mat4(0.0);
			finalTransformation = glm::mat4(0.0);
		}
	};
	struct VertexBoneData
	{
		GLuint ids[NUM_BONES_PER_VERTEX];
		float weights[NUM_BONES_PER_VERTEX];

		VertexBoneData() 
		{
			reset();
		}
		void reset()
		{
			for (GLuint i = 0; i < NUM_BONES_PER_VERTEX; i++)
			{
				ids[i] = 0;
				weights[i] = 0;
			}
		}
		void addBoneData(GLuint boneID, float weight);
	};

	void calcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	void calcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	void calcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	GLuint findScaling(float AnimationTime, const aiNodeAnim* pNodeAnim);
	GLuint findRotation(float AnimationTime, const aiNodeAnim* pNodeAnim);
	GLuint findPosition(float AnimationTime, const aiNodeAnim* pNodeAnim);
	const aiNodeAnim* findNodeAnim(const aiAnimation* pAnimation, const string NodeName);
	void readNodeHeirarchy(float AnimationTime, const aiNode* pNode, glm::mat4& ParentTransform);
	bool initFromScene(const aiScene* pScene, const char* Filename, TexturedModel* model);
	void initMesh(GLuint meshIndex,
		const aiMesh* paiMesh,
		std::vector<glm::vec3>& positions,
		std::vector<glm::vec3>& normals,
		std::vector<glm::vec2>& texCoords,
		std::vector<VertexBoneData>& bones,
		std::vector<GLuint>& indices,
		RawModel* model);
	void loadBones(GLuint meshIndex, const aiMesh* paiMesh, vector<VertexBoneData>& bones);
	bool initMaterials(const aiScene* pScene, const string& filename, ModelTexture** textures);
	void clear();

#define INVALID_MATERIAL 0xFFFFFFFF


enum VB_TYPES {
	INDEX_BUFFER,
	POS_VB,
	NORMAL_VB,
	TEXCOORD_VB,
	BONE_VB,
	NUM_VBs
};
	GLuint m_VAO;
	GLuint m_Buffers[NUM_VBs];
	Loader* loader;

	struct MeshEntry {
		MeshEntry()
		{
			NumIndices = 0;
			BaseVertex = 0;
			BaseIndex = 0;
			MaterialIndex = INVALID_MATERIAL;
		}

		unsigned int NumIndices;
		unsigned int BaseVertex;
		unsigned int BaseIndex;
		unsigned int MaterialIndex;
	};

	vector<MeshEntry> m_entries;
	vector<ModelTexture> m_textures;

	map<string, GLuint> m_boneMapping; // maps a bone name to its index
	GLuint m_numBones;
	vector<BoneInfo> m_boneInfo;
	glm::mat4 m_globalInverseTransform;

	const aiScene* m_pScene;
	Assimp::Importer m_Importer;
};
#endif /* !MODEL_H */