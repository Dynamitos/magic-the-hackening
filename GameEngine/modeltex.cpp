#include "modeltex.h"

ModelTexture::ModelTexture()
{
}

ModelTexture::ModelTexture(GLuint textureID)
	:diffuseID{ textureID }, displacementID{ 0 }, name { "" } {
	shineDamper = 1;
	numberOfRows = 1;
}
ModelTexture::ModelTexture(GLuint textureID, GLuint displacementID)
	: diffuseID{ textureID }, displacementID{ displacementID }, name{ "" } {
	shineDamper = 1;
	numberOfRows = 1;
}
ModelTexture::ModelTexture(GLuint textureID, char* name)
	: diffuseID{ textureID }, displacementID{ 0 }, name{ name } {
	shineDamper = 1;
	numberOfRows = 1;
}
ModelTexture::ModelTexture(GLuint textureID, GLuint displacementID, char* name)
	: diffuseID{ textureID }, displacementID{ displacementID }, name{ name } {
	shineDamper = 1;
	numberOfRows = 1;
}

ModelTexture::~ModelTexture()
{
	delete name;
}