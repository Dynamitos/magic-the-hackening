#ifndef MODELTEX_H
#define MODELTEX_H
#include <GL\glew.h>
class ModelTexture
{
public:
	ModelTexture();
	ModelTexture(GLuint textureID);
	ModelTexture(GLuint textureID, GLuint displacementID);
	ModelTexture(GLuint textureID, char* name);
	ModelTexture(GLuint textureID, GLuint displacementID, char* name);
	~ModelTexture();
	GLfloat shineDamper;
	GLfloat reflectivity;
	GLboolean hasTransparency;
	GLboolean useFakeLighting;
	GLint numberOfRows;
	GLuint diffuseID;
	GLuint specularID;
	GLuint ambientID;
	GLuint emissiveID;
	GLuint displacementID;
	char* name;
};
#endif /* !MODELTEX_H */