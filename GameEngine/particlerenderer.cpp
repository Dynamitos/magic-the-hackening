#include "particlerenderer.h"

ParticleRenderer::ParticleRenderer(glm::mat4 projectionMatrix, GLfloat* deltaTime)
{
	shader = new ParticleShader();
	shader->initShader();
	shader->start();
	shader->loadProjectionMatrix(projectionMatrix);
	shader->stop();
	particlesPositionData = (GLfloat*)flexAlloc(sizeof(GLfloat)*MAX_PARTICLES * 4 * 2);
	particlesVelocityData = (GLfloat*)flexAlloc(sizeof(GLfloat)*MAX_PARTICLES * 3 * 2);
	particlesColorData = (GLbyte*)flexAlloc(sizeof(GLbyte)*MAX_PARTICLES * 4 * 2);
	particlesSizeData = (GLfloat*)flexAlloc(sizeof(GLfloat)*MAX_PARTICLES * 2 * 2);
	particlesPhaseData = (GLint*)flexAlloc(sizeof(GLint)*MAX_PARTICLES * 2);
	particlesLifeData = (GLfloat*)flexAlloc(sizeof(GLfloat)*MAX_PARTICLES * 2);
	particlesActiveData = (GLint*)flexAlloc(sizeof(GLint)*MAX_PARTICLES * 2);
	particlesSourceData = (GLfloat*)flexAlloc(sizeof(GLfloat)*MAX_PARTICLES * 3 * 2);
	particlesDestinationData = (GLfloat*)flexAlloc(sizeof(GLfloat)*MAX_PARTICLES * 3 * 2);
	particlesShootingData = (bool*)flexAlloc(sizeof(bool)*MAX_PARTICLES * 2);
	GLfloat vertices[]{ -0.5, -0.5, 0.0,
		0.5, -0.5, 0.0,
		-0.5,  0.5, 0.0,
		0.5,  0.5, 0.0 };

	glGenVertexArrays(1, &vaoID);
	glBindVertexArray(vaoID);

	glGenBuffers(1, &billboardVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, billboardVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);

	glGenBuffers(1, &particlesPositionBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, particlesPositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * 4 * 2 * sizeof(GLfloat), malloc(MAX_PARTICLES * 4 * 2 * sizeof(GLfloat)), GL_STREAM_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, false, 0, 0);

	glGenBuffers(1, &particlesColorBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, particlesColorBuffer);
	glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * 4 * 2 * sizeof(GLbyte), malloc(MAX_PARTICLES * 4 * 2 * sizeof(GLbyte)), GL_STREAM_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 4, GL_BYTE, true, 0, 0);

	glGenBuffers(1, &particlesSizeBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, particlesSizeBuffer);
	glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * 2 * 2 * sizeof(GLfloat), malloc(MAX_PARTICLES * 2 * 2 * sizeof(GLfloat)), GL_STREAM_DRAW);
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 2, GL_FLOAT, false, 0, 0);

	glGenBuffers(1, &particlesLifeBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, particlesLifeBuffer);
	glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * 2 * sizeof(GLfloat), malloc(MAX_PARTICLES * 2 * sizeof(GLfloat)), GL_STREAM_DRAW);
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 1, GL_FLOAT, false, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	this->deltaTime = deltaTime;
	flexInit();
	clothSolver = flexCreateSolver(MAX_PARTICLES, 0);
	spellSolver = flexCreateSolver(MAX_PARTICLES, 0);
	params = new FlexParams();
	params->mRadius = 0.1;
	params->mDynamicFriction = 0.0f;
	params->mFluid = true;
	params->mViscosity = 0.0f;
	params->mNumIterations = 3;
	params->mVorticityConfinement = 40.f;
	params->mAnisotropyScale = 20.0f;
	params->mFluidRestDistance = 0.055;
	params->mSolidRestDistance = params->mFluidRestDistance;
	params->mNumPlanes = 5;
	params->mCollisionDistance = 0.05;
	params->mShapeCollisionMargin = params->mCollisionDistance*0.25;
	flexSetParams(spellSolver, params);
	srand(time(0));
}

ParticleRenderer::~ParticleRenderer()
{
	flexDestroySolver(clothSolver);
	flexShutdown();
	delete shader;
	delete particlesPositionData;
	delete particlesColorData;
	delete particlesSizeData;
	delete clothSolver;
	delete spellSolver;
}
void ParticleRenderer::render(Camera * camera)
{
	std::cout << activeSpells.size() << std::endl;
	for (std::vector<Spell*>::iterator it = (activeSpells).begin(); it != (activeSpells).end(); it++)
	{
		Spell* spell = *it;
		if (spell->finished || spell == 0)
		{
			spell = 0;
			continue;
		}
		addParticles(spell->invoke(*deltaTime));
	}
	int numParticles = 0;
	for (int i = 0; i < MAX_PARTICLES; i++)
	{
		if ((particlesLifeData[i] -= *deltaTime)>=0 && !particlesShootingData[i]) {
			particlesActiveData[numParticles] = i;
			particlesPositionData[i * 4 + 0] += particlesVelocityData[i * 3 + 0];
			particlesPositionData[i * 4 + 1] += particlesVelocityData[i * 3 + 1];
			particlesPositionData[i * 4 + 2] += particlesVelocityData[i * 3 + 2];
			numParticles++;
		}
		else
		{
			particlesLifeData[i] = 10;
			particlesShootingData[i] = true;
			particlesVelocityData[i * 3 + 0] = particlesDestinationData[i * 3 + 0] / 10;
			particlesVelocityData[i * 3 + 1] = particlesDestinationData[i * 3 + 1] / 10;
			particlesVelocityData[i * 3 + 2] = particlesDestinationData[i * 3 + 2] / 10;
		}
	}
	/*flexSetParticles(spellSolver, particlesPositionData, numParticles, eFlexMemoryHostAsync);
	flexSetVelocities(spellSolver, particlesVelocityData, numParticles, eFlexMemoryHostAsync);
	flexSetPhases(spellSolver, particlesPhaseData, numParticles, eFlexMemoryHostAsync);
	flexSetActive(spellSolver, particlesActiveData, numParticles, eFlexMemoryHostAsync);
	flexUpdateSolver(spellSolver, *deltaTime, 2, NULL);
	flexGetParticles(spellSolver, particlesPositionData, numParticles, eFlexMemoryHostAsync);
	flexGetVelocities(spellSolver, particlesVelocityData, numParticles, eFlexMemoryHostAsync);
	*/

	flexSetFence();
	flexWaitFence();
	shader->start();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 3);
	shader->loadTexture();
	shader->loadCamera(camera);
	glBindVertexArray(vaoID);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);

	glBindBuffer(GL_ARRAY_BUFFER, particlesPositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * 4 * 2, (void*)0, GL_STREAM_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, MAX_PARTICLES*4, particlesPositionData);


	glBindBuffer(GL_ARRAY_BUFFER, particlesColorBuffer);
	glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * 4 * 2, (void*)0, GL_STREAM_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, MAX_PARTICLES*4, particlesColorData);


	glBindBuffer(GL_ARRAY_BUFFER, particlesSizeBuffer);
	glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * 2 * 2, (void*)0, GL_STREAM_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, MAX_PARTICLES*2, particlesSizeData);

	glBindBuffer(GL_ARRAY_BUFFER, particlesLifeBuffer);
	glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * 2, (void*)0, GL_STREAM_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, MAX_PARTICLES * 2, particlesLifeData);

	glVertexAttribDivisor(0, 0);
	glVertexAttribDivisor(1, 1);
	glVertexAttribDivisor(2, 1);
	glVertexAttribDivisor(3, 1);
	glVertexAttribDivisor(4, 1);

	glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, MAX_PARTICLES);
	
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(4);

	glBindVertexArray(0);
	shader->stop();
}

void ParticleRenderer::addParticles(std::vector<Particle> particles)
{
	for (std::vector<Particle>::iterator it = particles.begin(); it != particles.end(); it++)
	{
		Particle* p = &*it;
		int index = findUnusedParticle();
		particlesPositionData[index * 4 + 0] = p->pos.x;
		particlesPositionData[index * 4 + 1] = p->pos.y;
		particlesPositionData[index * 4 + 2] = p->pos.z;
		particlesPositionData[index * 4 + 3] = p->mass;
		particlesColorData[index * 4 + 0] = p->r;
		particlesColorData[index * 4 + 1] = p->g;
		particlesColorData[index * 4 + 2] = p->b;
		particlesColorData[index * 4 + 3] = p->a;
		particlesSizeData[index * 2 + 0] = p->width;
		particlesSizeData[index * 2 + 1] = p->height;
		particlesVelocityData[index * 3 + 0] = p->speed.x;
		particlesVelocityData[index * 3 + 1] = p->speed.y;
		particlesVelocityData[index * 3 + 2] = p->speed.z;
		particlesPhaseData[index] = p->phase;
		particlesLifeData[index] = p->life;
		particlesShootingData[index] = false;
		particlesSourceData[index * 3 + 0] = p->source.x;
		particlesSourceData[index * 3 + 1] = p->source.y;
		particlesSourceData[index * 3 + 2] = p->source.z;
		particlesDestinationData[index * 3 + 0] = p->destination.x;
		particlesDestinationData[index * 3 + 1] = p->destination.y;
		particlesDestinationData[index * 3 + 2] = p->destination.z;
	}
}

float ParticleRenderer::calcAverage(GLfloat * music, int start, int end)
{
	return 0.0f;
}
int findUnusedParticle()
{
	for (int i = lastUsedParticle; i < MAX_PARTICLES; i++) {
		if (particlesLifeData[i] <= 0) {
			lastUsedParticle = i;
			return i;
		}
	}
	for (int i = 0; i < lastUsedParticle; i++) {
		if (particlesLifeData[i] <= 0) {
			lastUsedParticle = i;
			return i;
		}
	}
	return 0;
}
