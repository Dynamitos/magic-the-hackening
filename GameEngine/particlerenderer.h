#ifndef PARTICLERENDERER_H
#define PARTICLERENDERER_H
#include <time.h>
#include <vector>
#include <cloth\PxCloth.h>
#include "particleshader.h"

#define MAX_PARTICLES 100000

static int lastUsedParticle = 0;
static GLfloat* particlesPositionData;
static GLfloat* particlesVelocityData;
static GLbyte* particlesColorData;
static GLfloat* particlesSizeData;
static GLint* particlesPhaseData;
static GLfloat* particlesLifeData;
static GLint* particlesActiveData;
static bool* particlesShootingData;
static GLfloat* particlesSourceData;
static GLfloat* particlesDestinationData;
static int findUnusedParticle();
extern std::vector<Spell*> activeSpells;
class ParticleRenderer
{
private:
	ParticleShader* shader;
	GLuint billboardVertexBuffer;
	GLuint particlesPositionBuffer;
	GLuint particlesColorBuffer;
	GLuint particlesSizeBuffer;
	GLuint particlesLifeBuffer;
	GLuint vaoID;
	FlexSolver* clothSolver;
	FlexSolver* spellSolver;
	FlexParams* params;
	GLfloat* deltaTime;
	volatile bool music = false;
	float calcAverage(GLfloat * music, int start, int end);
public:
	ParticleRenderer(glm::mat4 projectionMatrix, GLfloat* deltaTime);
	~ParticleRenderer();
	void render(Camera * camera);
	void addParticles(std::vector<Particle> particles);

};
#endif /* !PARTICLERENDERER_H */