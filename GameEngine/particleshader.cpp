#include "particleshader.h"

ParticleShader::ParticleShader()
:ShaderProgram("particleVertex.shader", "particleFragment.shader"){
}

void ParticleShader::loadProjectionMatrix(glm::mat4& projectionMatrix)
{
	ShaderProgram::loadMatrix(location_projection, projectionMatrix);
}

void ParticleShader::loadTexture()
{
	ShaderProgram::loadInt(location_texture, 0);
}

void ParticleShader::loadCamera(Camera * camera)
{
	glm::mat4 viewMatrix = createViewMatrix(camera);
	ShaderProgram::loadMatrix(location_view, viewMatrix);
	ShaderProgram::loadVector(location_cameraRight, glm::vec3(viewMatrix[0][0], viewMatrix[1][0], viewMatrix[2][0]));
	ShaderProgram::loadVector(location_cameraUp, glm::vec3(viewMatrix[0][1], viewMatrix[1][1], viewMatrix[2][1]));
}

void ParticleShader::bindAttributes()
{
	ShaderProgram::bindAttribute(0, "squareVertices");
	ShaderProgram::bindAttribute(1, "xyzs");
	ShaderProgram::bindAttribute(2, "color");
	ShaderProgram::bindAttribute(3, "size");
}

void ParticleShader::getAllUniformLocations()
{
	location_cameraRight = ShaderProgram::getUniformLocation("cameraRight_worldspace");
	location_cameraUp = ShaderProgram::getUniformLocation("cameraUp_worldspace");
	location_projection = ShaderProgram::getUniformLocation("projectionMatrix");
	location_view = ShaderProgram::getUniformLocation("viewMatrix");
	location_texture = ShaderProgram::getUniformLocation("textureSampler");
}
