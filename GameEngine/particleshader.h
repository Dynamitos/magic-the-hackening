#ifndef PARTICLESHADER_H
#define PARTICLESHADER_H
#include "shader.h"
class ParticleShader : public ShaderProgram
{
public:
	ParticleShader();
	void loadProjectionMatrix(glm::mat4& projectionMatrix);
	void loadTexture();
	void loadCamera(Camera* camera);
protected:
	void bindAttributes();
	void getAllUniformLocations();
private:
	GLuint location_cameraRight;
	GLuint location_cameraUp;
	GLuint location_projection;
	GLuint location_view;
	GLuint location_texture;
};

#endif