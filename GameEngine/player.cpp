#include "player.h"

#define PI 3.14159
#define toRadians(x) x*PI/180

Player::Player(TexturedModel* model, glm::vec3 position, GLfloat rotX, GLfloat rotY, GLfloat rotZ, GLfloat scale, Status* status)
	:EntityMob{ model, position, rotX, rotY, rotZ, scale, status},
	RUN_SPEED{ 50 }, GRAVITY{ -50 }, JUMP_POWER{ 30 },
	terrainHeight{ 0 }, currentSpeed{ 0 }, currentStriveSpeed{ 0 },
	upwardsSpeed{ 0 }, isInAir{ false } {
}

void Player::move(bool keys[], float frameTime)
{
	checkInputs(keys);
	float striveDistance = currentStriveSpeed*frameTime;
	float dxStrive = (float)(striveDistance * sin(toRadians(rotY + 90)));
	float dzStrive = (float)(striveDistance * cos(toRadians(rotY + 90)));
	float distance = currentSpeed * frameTime;
	float dx = (float)(distance * sin(toRadians(rotY)));
	float dz = (float)(distance * cos(toRadians(rotY)));
	upwardsSpeed += GRAVITY * frameTime;
	Entity::move(dx + dxStrive, upwardsSpeed*frameTime, dz + dzStrive);
	if (position.y < terrainHeight)
	{
		upwardsSpeed = 0;
		position.y = terrainHeight;
		isInAir = false;
	}
}

void Player::jump()
{
	upwardsSpeed = JUMP_POWER;
	isInAir = true;
}

void Player::checkInputs(bool keys[])
{
	if (keys[FORWARD_KEY]) {
		currentSpeed = RUN_SPEED;
	}
	else if (keys[BACKWARD_KEY]) {
		currentSpeed = -RUN_SPEED;
	}
	else {
		currentSpeed = 0;
	}
	if (keys[LEFT_KEY]) {
		currentStriveSpeed = RUN_SPEED;
	}
	else if (keys[RIGHT_KEY]) {
		currentStriveSpeed = -RUN_SPEED;
	}
	else {
		currentStriveSpeed = 0;
	}
	if (keys[JUMP_KEY] && !isInAir) {
		jump();
	}
	if (keys[SPELL_KEY])
	{
		Element fire = Element();
		fire.a = 1;
		fire.r = 1;
		fire.g = 0;
		fire.b = 0;
		Spell* fireSpell = new Spell(fire, 100, 2, 2, glm::vec3(0, 0, 0), glm::vec3(0, 0, 1));
		(activeSpells).push_back(fireSpell);
	}
}
