#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <GL\glew.h>
#include "entitymob.h"
#ifndef DISPLAY_H
#include "display.h"
#endif
extern std::vector<Spell*> activeSpells;
class Player : public EntityMob
{
public:
	Player(TexturedModel* model, glm::vec3 position, GLfloat rotX, GLfloat rotY, GLfloat rotZ, GLfloat scale, Status* status);
	void move(bool keys[], float frameTime);
	void jump();
	void checkInputs(bool keys[]);
	
private:
	float RUN_SPEED;
	float GRAVITY;
	float JUMP_POWER;
	float terrainHeight;
	float currentSpeed;
	float currentStriveSpeed;
	float upwardsSpeed;
	bool isInAir;
	char* name;
	glm::vec3 lookingDirection;
};
#endif /* !PLAYER_H */