#include "rawmodel.h"

RawModel::RawModel(GLuint vaoID, GLsizei vertexCount) 
	: vaoID{vaoID}, vertexCount{vertexCount}, name{""}{
}
RawModel::RawModel(GLuint vaoID, GLsizei vertexCount, const char* name)
	: vaoID{ vaoID }, vertexCount{ vertexCount }, name{ name } {
}
RawModel::~RawModel()
{
	delete name;
}