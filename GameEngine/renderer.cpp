#include "renderer.h"

std::vector<Spell*> activeSpells;
MasterRenderer::MasterRenderer(Loader*  loader, GLfloat* deltaTime)
{
	this->deltaTime = deltaTime;
	FOV = 70;
	NEAR_PLANE = 0.1;
	FAR_PLANE = 1000;
	RENDER_DISTANCE = 5;
	SKY_COLOR = glm::vec3(0.4, 0.6, 0.7);
	density = 0.0035;
	gradient = 1;
	enableCulling();
	projectionMatrix = glm::perspective(FOV, (float)WIDTH / (float)HEIGHT, NEAR_PLANE, FAR_PLANE);
	entityRenderer = new EntityRenderer(projectionMatrix);
	particleRenderer = new ParticleRenderer(projectionMatrix, deltaTime);
}

void MasterRenderer::render(Camera * camera)
{

}
void MasterRenderer::render(Camera * camera, std::multimap<TexturedModel*, Entity*> entities)
{
	prepare();
	entityRenderer->render(camera, entities, glm::vec4(0, -1, 0, 10000));
	particleRenderer->render(camera);
}

void MasterRenderer::renderScene(Camera* camera, std::multimap<TextureData*, Entity*> entities, glm::vec4 clipPlane)
{
	prepare();
	
}

void MasterRenderer::renderGui()
{
}

void MasterRenderer::renderWater()
{
}

void MasterRenderer::prepare()
{
	glEnable(GL_DEPTH_TEST);
	glClearColor(SKY_COLOR.x, SKY_COLOR.y, SKY_COLOR.z, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

MasterRenderer::~MasterRenderer()
{
	delete entityRenderer;
	delete particleRenderer;
}
