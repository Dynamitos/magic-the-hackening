#ifndef MASTERRENDERER_H
#define MASTERRENDERER_H

#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <iostream>
#include <glm\glm.hpp>
#include "util.h"
#include "texmodel.h"
#include "loader.h"
#include "data.h"
#include "entityrenderer.h"
#include "particlerenderer.h"
extern bool keys[65536];
extern std::vector<Spell*> activeSpells;
class MasterRenderer
{
public:
	void render(Camera* camera);
	void render(Camera* camera, std::multimap<TexturedModel*, Entity*> entities);
	void renderScene(Camera* camera, std::multimap<TextureData*, Entity*> entities, glm::vec4 clipPlane);
	void renderGui();
	void renderWater();
	void prepare();
	MasterRenderer(Loader* loader, GLfloat* deltaTime);
	~MasterRenderer();
private:
	GLfloat* deltaTime;
	glm::mat4 projectionMatrix;
	EntityRenderer* entityRenderer;
	ParticleRenderer* particleRenderer;
};
#endif