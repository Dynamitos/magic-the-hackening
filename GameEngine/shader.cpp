#include "shader.h"

ShaderProgram::ShaderProgram(char* vertexShader, char* fragmentShader, char* tesselationControlShader, char* tesselationEvaluationShader)
{
	vertexShaderID = loadShader(vertexShader, GL_VERTEX_SHADER);
	fragmentShaderID = loadShader(fragmentShader, GL_FRAGMENT_SHADER);
	tesselationControlShaderID = loadShader(tesselationControlShader, GL_TESS_CONTROL_SHADER);
	tesselationEvaluationShaderID = loadShader(tesselationEvaluationShader, GL_TESS_EVALUATION_SHADER);
	programID = glCreateProgram();
	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);
	glAttachShader(programID, tesselationControlShaderID);
	glAttachShader(programID, tesselationEvaluationShaderID);

}
ShaderProgram::ShaderProgram(char* vertexShader, char* fragmentShader)
{
	vertexShaderID = loadShader(vertexShader, GL_VERTEX_SHADER);
	fragmentShaderID = loadShader(fragmentShader, GL_FRAGMENT_SHADER);
	programID = glCreateProgram();
	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);
}
ShaderProgram::~ShaderProgram() 
{
	glDetachShader(programID, vertexShaderID);
	glDetachShader(programID, fragmentShaderID);
	glDetachShader(programID, tesselationControlShaderID);
	glDetachShader(programID, tesselationEvaluationShaderID);
	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);
	glDeleteShader(tesselationControlShaderID);
	glDeleteShader(tesselationEvaluationShaderID);
	glDeleteProgram(programID);
}
void ShaderProgram::initShader()
{
	bindAttributes();
	glLinkProgram(programID);
	glValidateProgram(programID);
	getAllUniformLocations();
}
void ShaderProgram::start() 
{
	glUseProgram(programID);
}
void ShaderProgram::stop() 
{
	glUseProgram(0);
}
GLint ShaderProgram::getUniformLocation(const char* uniformName)
{
	GLint location = glGetUniformLocation(programID, uniformName);
	if (location == -1)
		std::cout << "Uniform " << uniformName << " not found" << std::endl;
	return location;
}
void ShaderProgram::loadMatrix(GLuint location, glm::mat4 matrix)
{
	glUniformMatrix4fv(location, 1, GL_FALSE, &matrix[0][0]);
}
void ShaderProgram::loadInt(GLuint location, GLint value)
{
	glUniform1i(location, value);
}
void ShaderProgram::loadFloat(GLuint location, GLfloat value)
{
	glUniform1f(location, value);
}
void ShaderProgram::loadBoolean(GLuint location, GLboolean value)
{
	glUniform1f(location, value);
}
void ShaderProgram::loadVector(GLuint location, glm::vec3 vector)
{
	glUniform3f(location, vector.x, vector.y, vector.z);
}
void ShaderProgram::loadVector(GLuint location, GLfloat x, GLfloat y)
{
	glUniform2f(location, x, y);
}
void ShaderProgram::loadVector(GLuint location, glm::vec2 value)
{
	glUniform2f(location, value.x, value.y);
}
void ShaderProgram::loadVector(GLuint location, glm::vec4 value)
{
	glUniform4f(location, value.x, value.y, value.z, value.w);
}
void ShaderProgram::bindAttribute(GLuint attributeNumber, char* attributeName)
{
	glBindAttribLocation(programID, attributeNumber, attributeName);
}
std::string readFile(const char* filePath) 
{
	std::string content;
	std::ifstream fileStream(filePath, std::ios::in);


	if (!fileStream.is_open()) {
		return "";
	}

	std::string line = "";
	while (!fileStream.eof()) {
		std::getline(fileStream, line);
		content.append(line + "\n");
	}

	fileStream.close();
	return content;
}
GLuint ShaderProgram::loadShader(char* file, GLuint type) 
{
	std::string path;
	path.append(SHADER_PATH);
	path.append(file);
	std::string str = readFile(path.c_str());
	const char* source = str.c_str();
	GLuint shaderID = glCreateShader(type);
	glShaderSource(shaderID, 1, &source, NULL);
	glCompileShader(shaderID);
	GLint result;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
	if (result != GL_TRUE)
	{
		GLint infoLogLength;
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLogLength);

		GLchar* strInfoLog = new GLchar[infoLogLength + 1];
		glGetShaderInfoLog(shaderID, infoLogLength, NULL, strInfoLog);
		std::cout << strInfoLog << std::endl;
	}

	return shaderID;
}
