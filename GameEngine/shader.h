#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H
#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <glm\glm.hpp>
#include <GL\glew.h>
#include "cam.h"
#include "util.h"
#include "maths.h"
class ShaderProgram
{
public:
	ShaderProgram(char* vertexShader, char* fragmentShader, char* tesselationControlShader, char* tesselationEvaluationShader);
	ShaderProgram(char* vertexShader, char* fragmentShader);
	~ShaderProgram();
	void start();
	void stop();
	void initShader();
protected:
	virtual void bindAttributes() = 0;
	virtual void getAllUniformLocations() = 0;
	void bindAttribute(GLuint attributeNumber, char* attributeName);
	GLint getUniformLocation(const char* uniformName);
	void loadMatrix(GLuint location, glm::mat4 matrix);
	void loadInt(GLuint location, GLint value);
	void loadFloat(GLuint location, GLfloat value);
	void loadBoolean(GLuint location, GLboolean value);
	void loadVector(GLuint location, glm::vec3 vector);
	void loadVector(GLuint location, GLfloat x, GLfloat y);
	void loadVector(GLuint location, glm::vec2 value);
	void loadVector(GLuint location, glm::vec4 value);
private:
	GLuint programID;
	GLuint vertexShaderID;
	GLuint fragmentShaderID;
	GLuint tesselationControlShaderID;
	GLuint tesselationEvaluationShaderID;
	static GLuint loadShader(char* file, GLuint type);
};

#endif /* !SHADERPROGRAM_H */