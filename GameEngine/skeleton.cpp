#include "skeleton.h"

Skeleton::Skeleton(TexturedModel* model, glm::vec3 position, GLfloat rotX, GLfloat rotY, GLfloat rotZ, GLfloat scale, Entity* player, Status* status)
	:EntityMob(model, position, rotX, rotY, rotZ, scale, status),
	RUN_SPEED{ 20 }, GRAVITY{ -50 }, JUMP_POWER{ 0 },
	player{ player }
{}


void Skeleton::move(float frameTime)
{
	glm::vec3 direction = this->getPosition() - player->getPosition();
	glm::normalize(direction);
	direction *= RUN_SPEED * frameTime;
	if (player->getPosition() + direction*(float)200 == this->getPosition())
		return;
	Entity::move(direction.x, direction.y, direction.z);
}