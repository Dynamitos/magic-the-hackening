#ifndef SKELETON_H
#define SKELETON_H

#include "entitymob.h"

class Skeleton : public EntityMob
{
	public:
		Skeleton(TexturedModel* model, glm::vec3 position, GLfloat rotX, GLfloat rotY, GLfloat rotZ, GLfloat scale, Entity* player, Status* status);
		void move(float frameTime);
	private:
		float RUN_SPEED;
		float GRAVITY;
		float JUMP_POWER;
		Entity* player;
};
#endif /* !SKELETON_H */