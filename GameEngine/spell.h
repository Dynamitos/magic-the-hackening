#ifndef SPELL_H
#define SPELL_H

#include "element.h"
#include <GL\glew.h>
#include <glm\glm.hpp>

class Spell
{
	public:
		Spell(Element* element, EntityMob* entity);
		~Spell();
		void charge();
	private:
		Element* element;
		EntityMob* caster;
		void invoke();
};

#endif // !SPELL_H

