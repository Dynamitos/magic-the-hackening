#include "spider.h"

Spider::Spider(TexturedModel* model, glm::vec3 position, GLfloat rotX, GLfloat rotY, GLfloat rotZ, GLfloat scale, Entity* player, Status* status)
	:EntityMob{ model, position, rotX, rotY, rotZ, scale, status},
	RUN_SPEED{ 30 }, GRAVITY{ -50 }, JUMP_POWER{ 0 },
	player{ player }
{}
Spider::~Spider()
{}

void Spider::move(float frameTime)
{
	glm::vec3 direction = this->getPosition() - player->getPosition();
	glm::normalize(direction);
	direction *= RUN_SPEED * frameTime;
	/*
	Speciality still missing
	*/
	Entity::move(direction.x, direction.y, direction.z);
}