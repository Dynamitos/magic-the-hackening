#ifndef SPIDER_H
#define SPIDER_H

#include "entitymob.h"

class Spider : public EntityMob
{
	public:
		Spider(TexturedModel* model, glm::vec3 position, GLfloat rotX, GLfloat rotY, GLfloat rotZ, GLfloat scale, Entity* player, Status* status);
		~Spider();
		void move(float frameTime);
	private:
		float RUN_SPEED;
		float GRAVITY;
		float JUMP_POWER;
		Entity* player;
};
#endif // !SPIDER_H

