#include "status.h"

Status::Status(float hitpoints, float hp_reg, float damage, float intensity)
	:HP{ hitpoints }, HP_REG{ hp_reg }, DAMAGE{ damage }, INTENSITY{ intensity }, isAlive{ true },
	current_hp{hitpoints}
{}
