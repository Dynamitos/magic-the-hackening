#ifndef STATUS_H
#define STATUS_H

class Status
{
public:
	float HP;
	float HP_REG;
	float DAMAGE;
	float INTENSITY;
	float current_hp;

	bool isAlive;

	Status(float hitpoints, float hp_reg, float damage, float intensity);
};

#endif // !STATUS_H

