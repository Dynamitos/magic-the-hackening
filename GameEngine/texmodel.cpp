#include "texmodel.h"

TexturedModel::TexturedModel(RawModel** models, GLuint numModels, ModelTexture** textures, GLuint numTextures, GLfloat* currentTime)
	:models{ models }, numModels{ numModels }, textures{ textures }, numTextures{ numTextures }, currentTime{ currentTime }
{
	currentAnimation = 0;
}
TexturedModel::~TexturedModel()
{
	delete models;
	delete textures;
	delete animationTicks;
	delete animationDurations;
}
RawModel** TexturedModel::getModels()
{
	return models;
}
ModelTexture** TexturedModel::getTextures()
{
	return textures;
}
void TexturedModel::setModels(RawModel ** models)
{
	this->models = models;
}
void TexturedModel::setTextures(ModelTexture ** textures)
{
	this->textures = textures;
}
void TexturedModel::setNumModels(GLuint numModels)
{
	this->numModels = numModels;
}
void TexturedModel::setNumTextures(GLuint numTextures)
{
	this->numTextures = numTextures;
}
GLuint TexturedModel::getNumModels()
{
	return numModels;
}
GLuint TexturedModel::getNumTextures()
{
	return numTextures;
}

void TexturedModel::setAnimations(aiAnimation** animations, GLuint numAnimations)
{
	this->numAnimations = numAnimations;
	this->animationDurations = (GLfloat*)malloc(sizeof(GLfloat)*numAnimations);
	this->animationTicks = (GLfloat*)malloc(sizeof(GLfloat)*numAnimations);
	for (int i = 0; i < numAnimations; i++)
	{
		animationTicks[i] = animations[i]->mTicksPerSecond;
		animationDurations[i] = animations[i]->mDuration;
	}
}

void TexturedModel::setCurrentAnimation(std::string animationName)
{
	currentAnimation = animations[animationName];
	timeStamp = *currentTime;
}

void TexturedModel::setSkeleton(std::vector<Bone*> skeleton)
{
	this->skeleton = skeleton;
}

GLuint TexturedModel::update(glm::mat4* matrices)
{
	float deltaTime = *currentTime - timeStamp;
	double ticksPerSecond = animationTicks[currentAnimation];
	double length = animationDurations[currentAnimation];
	while(deltaTime>=length)
	{
			deltaTime -= length;
	}
	int i = 0;
	for (std::vector<Bone*>::iterator it = skeleton.begin(); it != skeleton.end(); it++)
	{
		Bone* b = *it;
		std::map<double, glm::mat4>::iterator low;
		glm::mat4 translation, rotation;
		low = b->positionMaps[currentAnimation].lower_bound(deltaTime);
		if (low == b->positionMaps[currentAnimation].end())
		{
			translation = glm::mat4(1);
		}
		else {
			translation = low->second;
		}
		low = b->rotationMaps[currentAnimation].lower_bound(deltaTime);
		if (low == b->rotationMaps[currentAnimation].end())
		{
			rotation = glm::mat4(1);
		}
		else {
			rotation = low->second;
		}
		matrices[i] = glm::mat4(1);//translation*rotation;
		i++;
	}
	return i;
}