#ifndef TEXTUREDMODEL_H
#define TEXTUREDMODEL_H
#include <map>
#include <string>
#include <vector>

#include "modeltex.h"
#include "rawmodel.h"

#include <glm\glm.hpp>
#include <glm\gtx\matrix_interpolation.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <assimp\Importer.hpp>
#include <assimp\postprocess.h>
#include <assimp\scene.h>
#include <iostream>

#define NUM_BONES_PER_VERTEX 4


struct Bone
{
	glm::mat4 relativeTransform, absoluteTransform, globalTransform, invBindPose, bindPose;
	GLint id;
	std::vector<std::map<double, glm::mat4>> positionMaps;
	std::vector<std::map<double, glm::mat4>> rotationMaps;
	std::vector<std::map<double, glm::mat4>> scaleMaps;
	Bone* parent;
	bool operator<(const Bone* other)
	{
		return this->id < other->id;
	}
};

class TexturedModel
{
public:
	TexturedModel(RawModel** model, GLuint numModels, ModelTexture** texture, GLuint numTextures, GLfloat* currentTime);
	~TexturedModel();
	RawModel** getModels();
	ModelTexture** getTextures();
	void setModels(RawModel** models);
	void setTextures(ModelTexture** textures);
	void setNumModels(GLuint numModels);
	void setNumTextures(GLuint numTextures);
	GLuint getNumModels();
	GLuint getNumTextures();
	void setCurrentAnimation(std::string animationName);
	void setSkeleton(std::vector<Bone*> skeleton);
	void setAnimations(aiAnimation** animations, GLuint numAnimations);
	GLuint update(glm::mat4* matrices);
	std::map<std::string, GLuint> animations;
private:
	GLuint currentAnimation = -1;
	std::vector<Bone*> skeleton;
	GLfloat timeStamp;
	GLfloat* currentTime;
	RawModel** models;
	ModelTexture** textures;
	GLuint numModels;
	GLuint numTextures;
	GLfloat* animationTicks;
	GLfloat* animationDurations;
	GLuint numAnimations;
};
#endif /* !TEXTUREDMODEL_H */