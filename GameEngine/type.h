#ifndef TYPE_H
#define TYPE_H

#include "entitymob.h"

class Type
{
	public:
		virtual glm::vec3 updateParticle(EntityMob* caster, glm::vec3 position, glm::vec3 speed) = 0;
};

class Shield : public Type
{
	public:
		glm::vec3 updateParticle(EntityMob* caster, glm::vec3 position, glm::vec3 speed) override;
};

class Arrow : public Type
{
	public:
		glm::vec3 updateParticle(EntityMob* caster, glm::vec3 position, glm::vec3 speed) override;
};

class ShockWave : public Type
{
	public:
		glm::vec3 updateParticle(EntityMob* caster, glm::vec3 position, glm::vec3 speed) override;
};

/*
class Counter : public Type
{
public:
glm::vec3 updateParticle(EntityMob* caster, glm::vec3 position, glm::vec3 speed) override;
};

class Cone : public Type
{
public:
glm::vec3 updateParticle(EntityMob* caster, glm::vec3 position, glm::vec3 speed) override;
};

class Homing : public Type
{
public:
glm::vec3 updateParticle(EntityMob* caster, glm::vec3 position, glm::vec3 speed) override;
};

class SelfCast : public Type
{
public:
glm::vec3 updateParticle(EntityMob* caster, glm::vec3 position, glm::vec3 speed) override;
};
*/
#endif /*!TYPE_H*/
