#ifndef UTIL_H
#define UTIL_H
#include <glm\glm.hpp>
#include <GL\glew.h>
#include "light.h"

#define MAX_LIGHTS 4


static void enableCulling() {
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
}
static void disableCulling() {
	glDisable(GL_CULL_FACE);
}


static glm::vec3 SKY_COLOR;
static Light* lights[MAX_LIGHTS];
static float density, gradient;

static GLfloat currentTime;

static const char* RES_PATH = "C:\\Program Files\\Java\\res\\";
static const char* SHADER_PATH = "C:\\Program Files\\Java\\res\\shader\\";
static float FOV;
static float NEAR_PLANE;
static int RENDER_DISTANCE;
static float FAR_PLANE;
#endif /* !UTIL_H */