#ifndef ZOMBIE_H
#define ZOMBIE_H

#include "entitymob.h"

class Zombie : public EntityMob
{
	public :
		Zombie(TexturedModel* model, glm::vec3 position, GLfloat rotX, GLfloat rotY, GLfloat rotZ, GLfloat scale, Entity* player, Status* status);
		~Zombie();
		void move(float frameTime);
	private:
		float RUN_SPEED;
		float GRAVITY;
		float JUMP_POWER;
		Entity* player;
};
#endif /* !ZOMBIE_H */