#include "cam.h"

#define PI 3.14159
#define toRadians(x) x*PI/180

void Camera::calculateCameraPosition(float horizontalDistance, float verticalDistance)
{
	float theta = angleAroundPlayer;
	float xOffset = (float)(horizontalDistance * sin(toRadians(theta)));
	float yOffset = (float)(horizontalDistance * cos(toRadians(theta)));
	position.x = player->getPosition().x - xOffset;
	position.z = player->getPosition().z - yOffset;
	position.y = player->getPosition().y + verticalDistance;
	//TODO add terrain calculation
}

float Camera::calculateHorizontalDistance()
{
	return (float)(distanceFromPlayer * cos(toRadians(pitch)));
}

float Camera::calculateVerticalDistance()
{
	return (float)(distanceFromPlayer * sin(toRadians(pitch)));
}

void Camera::calculateZoom()
{
	float zoomLevel = (float)rotation * 2;
	rotation = 0;
	distanceFromPlayer -= zoomLevel;
	if (distanceFromPlayer < 2)
		distanceFromPlayer = 2;
}

void Camera::calculatePitch()
{
	float pitchChange = deltaY * 0.3;
	pitch += pitchChange;
	if (pitch > 90)
		pitch = 90;
	if (pitch < -90)
		pitch = -90;
}

void Camera::calculateAngleAroundPlayer()
{
	float angleChange = deltaX * 0.3;
	angleAroundPlayer -= angleChange;
	yaw += angleChange;
	player->rotY -= angleChange;
}

Camera::Camera(Player * player)
	:position{ glm::vec3(0, 1, 0) }, player{ player }, distanceFromPlayer{ 50 }, yaw{ 180 - angleAroundPlayer } {
}

void Camera::move()
{
	calculateAngleAroundPlayer();
	calculatePitch();
	calculateZoom();
	float horizontalDistance = calculateHorizontalDistance();
	float verticalDistance = calculateVerticalDistance();
	calculateCameraPosition(horizontalDistance, verticalDistance);

}
