#ifndef CAMERA_H
#define CAMERA_H
#include <math.h>

class Camera
{
private:
	float distanceFromPlayer;
	float angleAroundPlayer;
	float roll;
	Player* player;
	float terrainHeigth;
	void calculateCameraPosition(float horizontalDistance, float verticalDistance);
	float calculateHorizontalDistance();
	float calculateVerticalDistance();
	void calculateZoom();
	void calculatePitch();
	void calculateAngleAroundPlayer();
public:
	glm::vec3 position;
	float pitch;
	float yaw;
	Camera(Player* player);
	void move();
	Player* getPlayer() { return player; }
	void invertPitch() { pitch = -pitch; }
	void setY(float y) { position.y = y; }
};

#endif /* !CAMERA_H */