#ifndef DISPLAY_H
#define DISPLAY_H
static int CLOSE_KEY = GLFW_KEY_ESCAPE;
static int JUMP_KEY = GLFW_KEY_SPACE;
static int FORWARD_KEY = GLFW_KEY_W;
static int BACKWARD_KEY = GLFW_KEY_S;
static int LEFT_KEY = GLFW_KEY_A;
static int RIGHT_KEY = GLFW_KEY_D;


void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
	rotation = yoffset;
}
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == -1)
		return;
	DisplayManager::keys[key] = action != GLFW_RELEASE;
	std::cout << keys << std::endl;
}

public:
	GLFWwindow* window;
	//static Input input;
	//static MouseInput mouseWheel;
	float deltaX, deltaY;
	double rotation;
	bool buffering;
	int const WIDTH = 3840 / 4;
	int const HEIGHT = 2160 / 4;
	double lastFrameTime;
	GLfloat delta;
	double newX;
	double newY;
	bool mouseLocked;
	float mouseSpeed;
	bool keys[65536];

	
	void updateMouse() {
		if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS) {
			glfwSetCursorPos(window, WIDTH / 2, HEIGHT / 2);
			mouseLocked = true;
			//TODO add MasterRenderer shooting
		}
		else {
			//TODO add MasterRenderer shooting
		}
		if (mouseLocked) {
			double x, y;
			glfwGetCursorPos(window, &x, &y);
			newX = x;
			newY = y;
			deltaX = (float)(newX - (WIDTH / 2))*mouseSpeed;
			deltaY = (float)(newY - (HEIGHT / 2))*mouseSpeed;
			glfwSetCursorPos(window, WIDTH / 2, HEIGHT / 2);
		}
	}
	void createDisplay() {
		std::cout << keys << std::endl;
		newX = WIDTH / 2;
		newY = HEIGHT / 2;
		mouseLocked = false;
		mouseSpeed = 0.1f;
		buffering = false;
		glewExperimental = true;
		if (!glfwInit()) {
			std::cout << "FAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACK" << std::endl;
		}
		glfwWindowHint(GLFW_SAMPLES, 4);
		window = glfwCreateWindow(WIDTH, HEIGHT, "FINALLY", NULL, NULL);
		glfwShowWindow(window);
		glfwMakeContextCurrent(window);
		glfwSwapInterval(60);
		lastFrameTime = glfwGetTime();
		glewInit();
		glfwSetKeyCallback(window, key_callback);
		glfwSetScrollCallback(window, scroll_callback);
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	}
	void updateDisplay() {
		glfwPollEvents();
		updateMouse();
		glfwSwapBuffers(window);
		double currentFrameTime = glfwGetTime();
		delta = (float)(currentFrameTime - lastFrameTime);
		currentTime += delta;
		lastFrameTime = currentFrameTime;
	}
	void closeDisplay() {
		glfwDestroyWindow(window);
		glfwTerminate();
	}
	float getFrameTimeSeconds() {
		return delta;
	}
#endif /* !DISPLAY_H */