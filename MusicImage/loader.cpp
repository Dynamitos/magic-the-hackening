#include "loader.h"

Loader::Loader() {
	ilInit();
	glEnable(GL_TEXTURE_2D);
}

Loader::~Loader()
{
	cleanUp();
}
RawModel * Loader::loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat normals[], GLuint normalsLength, GLuint * boneIDs, GLuint boneIDLength, GLfloat * boneWeights, GLuint boneWeightsLength, GLuint indices[], GLuint indicesLength)
{
	GLuint vaoID = createVAO();
	bindIndicesBuffer(indices, indicesLength);
	storeDataInAttributeList(0, positions, positionsLength, 3);
	storeDataInAttributeList(1, texCoords, texCoordsLength, 2);
	storeDataInAttributeList(2, normals, normalsLength, 3);
	storeDataInAttributeList(3, boneIDs, boneIDLength, 4);
	storeDataInAttributeList(4, boneWeights, boneWeightsLength, 4);
	unbindVAO();
	return new RawModel(vaoID, indicesLength / sizeof(GLuint));
}
RawModel * Loader::loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat normals[], GLuint normalsLength, GLuint * boneIDs, GLuint boneIDLength, GLfloat * boneWeights, GLuint boneWeightsLength, GLuint indices[], GLuint indicesLength, const char * name)
{
	GLuint vaoID = createVAO();
	bindIndicesBuffer(indices, indicesLength);
	storeDataInAttributeList(0, positions, positionsLength, 3);
	storeDataInAttributeList(1, texCoords, texCoordsLength, 2);
	storeDataInAttributeList(2, normals, normalsLength, 3);
	storeDataInAttributeList(3, boneIDs, boneIDLength, 4);
	storeDataInAttributeList(4, boneWeights, boneWeightsLength, 4);
	unbindVAO();
	return new RawModel(vaoID, indicesLength / sizeof(GLuint), name);
}
RawModel* Loader::loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat normals[], GLuint normalsLength, GLuint indices[], GLuint indicesLength)
{
	GLuint vaoID = createVAO();
	bindIndicesBuffer(indices, indicesLength);
	storeDataInAttributeList(0, positions, positionsLength, 3);
	storeDataInAttributeList(1, texCoords, texCoordsLength, 2);
	storeDataInAttributeList(2, normals, normalsLength, 3);
	unbindVAO();
	return new RawModel(vaoID, indicesLength/sizeof(GLuint));
}
RawModel* Loader::loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat normals[], GLuint normalsLength, GLuint indices[], GLuint indicesLength, const char* name)
{
	GLuint vaoID = createVAO();
	bindIndicesBuffer(indices, indicesLength);
	storeDataInAttributeList(0, positions, positionsLength, 3);
	storeDataInAttributeList(1, texCoords, texCoordsLength, 2);
	storeDataInAttributeList(2, normals, normalsLength, 3);
	unbindVAO();
	return new RawModel(vaoID, indicesLength/sizeof(GLuint), name);
}
RawModel * Loader::loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat vertexColors[], GLuint vertexColorLength, GLfloat normals[], GLuint normalsLength, GLuint indices[], GLuint indicesLength)
{
	GLuint vaoID = createVAO();
	bindIndicesBuffer(indices, indicesLength);
	storeDataInAttributeList(0, positions, positionsLength, 3);
	storeDataInAttributeList(1, texCoords, texCoordsLength, 2);
	storeDataInAttributeList(2, normals, normalsLength, 4);
	storeDataInAttributeList(3, positions, positionsLength, 3);
	unbindVAO();
	return new RawModel(vaoID, indicesLength / sizeof(GLuint));
}
RawModel * Loader::loadToVAO(GLfloat positions[], GLuint positionsLength, GLfloat texCoords[], GLuint texCoordsLength, GLfloat vertexColors[], GLuint vertexColorLength, GLfloat normals[], GLuint normalsLength, GLuint indices[], GLuint indicesLength, const char * name)
{
	GLuint vaoID = createVAO();
	bindIndicesBuffer(indices, indicesLength);
	storeDataInAttributeList(0, positions, positionsLength, 3);
	storeDataInAttributeList(1, positions, positionsLength, 2);
	storeDataInAttributeList(2, positions, positionsLength, 4);
	storeDataInAttributeList(3, positions, positionsLength, 3);
	unbindVAO();
	return new RawModel(vaoID, indicesLength / sizeof(GLuint), name);
}
RawModel* Loader::loadToVAO(GLfloat positions[], GLuint positionsLength, GLint dimensions)
{
	GLuint vaoID = createVAO();
	storeDataInAttributeList(0, positions, positionsLength, dimensions);
	unbindVAO();
	return new RawModel(vaoID, positionsLength / sizeof(GLfloat) / dimensions);
}
RawModel* Loader::loadToVAO(GLfloat positions[], GLuint positionsLength, GLint dimensions, char* name)
{
	GLuint vaoID = createVAO();
	storeDataInAttributeList(0, positions, positionsLength, dimensions);
	unbindVAO();
	return new RawModel(vaoID, positionsLength / sizeof(GLfloat) / dimensions, name);
}
TextureData* Loader::decodeTextureFile(const char* fileName)
{
	int width = 0;
	int height = 0;
	ILuint image;
	ilGenImages(1, &image);
	ilBindImage(image);
	ilLoadImage(fileName);
	ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
	width = ilGetInteger(IL_IMAGE_WIDTH);
	height = ilGetInteger(IL_IMAGE_HEIGHT);
	GLubyte* data = ilGetData();
	ILenum error;
	if ((error = ilGetError()) != IL_NO_ERROR)
	{
		std::cout << error << std::endl;
		return nullptr;
	}
	return new TextureData(data, width*height*4, width, height);
}
GLuint Loader::loadCubeMap(const char * textureFiles[])
{
	GLuint texID;
	glGenTextures(1, &texID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texID);
	for (int i = 0; i < 6; i++) {
		TextureData* data = decodeTextureFile(textureFiles[i]);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, data->getWidth(), data->getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, data->getBuffer());
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	textures.push_back(texID);
	return texID;
}
GLuint Loader::loadTexture(const char * fileName)
{
	if (std::string(fileName).empty())
	{
		return loadTexture({ 0 }, 1);
	}
	TextureData* data = decodeTextureFile(fileName);
	GLuint texID;
	glGenTextures(1, &texID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, data->getWidth(), data->getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, data->getBuffer());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -0.4f);
	textures.push_back(texID);
	return texID;
}
GLuint Loader::loadTexture(GLubyte pixels[], int width)
{
	GLuint textureID;
	glGenTextures(1, &textureID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, width, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -0.4f);
	textures.push_back(textureID);
	return textureID;
}

TexturedModel * Loader::loadMesh(const char * fileName)
{
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(fileName, aiProcess_FlipUVs | aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_GenUVCoords | aiProcessPreset_TargetRealtime_MaxQuality);
	RawModel** models = (RawModel**)malloc(sizeof(RawModel*)*scene->mNumMeshes);
	ModelTexture** textures = (ModelTexture**)malloc(sizeof(ModelTexture*)*scene->mNumMaterials);
	aiNode* rootNode = scene->mRootNode;
	std::vector<Bone*> skeleton;
	std::map<const char*, int> necessityMap;
	Bone* root = new Bone();
	aiMatrix4x4 temp = rootNode->mTransformation;
	root->globalTransform = glm::mat4(temp.a1, temp.a2, temp.a3, temp.a4,
		temp.b1, temp.b2, temp.b3, temp.b4,
		temp.c1, temp.c2, temp.c3, temp.c4,
		temp.d1, temp.d2, temp.d3, temp.d4);
	root->parent = root;
	skeleton.push_back(root);
	/*loadBones(skeleton, rootNode, 0, necessityMap);
	std::vector<glm::mat4> bindPose(skeleton.size());
	std::vector<glm::mat4> invBindPose(skeleton.size());
	for (size_t i = 0; i < skeleton.size(); i++)
	{
		bindPose[i] = (skeleton[i].transform);
		invBindPose[i] = glm::inverse(bindPose[i]);
	}
	std::vector<glm::mat4> skinnedXForm(skeleton.size());
	for (size_t i = 0; i < skeleton.size();i++) {
		Bone* b = skeleton[i];
		if (b->parent == b)
		{
			b->transform = b->nodeTransform;
		}
		else {
			b.transform = skeleton[b.parent].transform * b.nodeTransform;
		}
	}*/
	for (size_t i = 0; i < scene->mNumMaterials; i++)
	{
		ModelTexture* temp = new ModelTexture();
		aiMaterial* material = scene->mMaterials[i];
		aiString string;

		material->Get(AI_MATKEY_TEXTURE_AMBIENT(0), string);
		std::string tempStr = std::string(string.C_Str());
		std::stringstream ss;
		if (tempStr != "") {
			tempStr = tempStr.substr(tempStr.find_last_of("/")+1);
			ss << RES_PATH << tempStr;
		}
		temp->ambientID = loadTexture(ss.str().c_str());

		material->Get(AI_MATKEY_TEXTURE_DIFFUSE(0), string);
		tempStr = std::string(string.C_Str());
		ss.str(std::string());
		if (tempStr != "") {
			tempStr = tempStr.substr(tempStr.find_last_of("/")+1);
			ss << RES_PATH << tempStr;
		}
		temp->diffuseID = loadTexture(ss.str().c_str());

		material->Get(AI_MATKEY_TEXTURE_EMISSIVE(0), string);
		tempStr = std::string(string.C_Str());
		ss.str(std::string());
		if (tempStr != "") {
			tempStr = tempStr.substr(tempStr.find_last_of("/")+1);
			ss << RES_PATH << tempStr;
		}
		temp->emissiveID = loadTexture(ss.str().c_str());

		material->Get(AI_MATKEY_TEXTURE_DISPLACEMENT(0), string);
		tempStr = std::string(string.C_Str());
		ss.str(std::string());
		if (tempStr != "") {
			tempStr = tempStr.substr(tempStr.find_last_of("/")+1);
			ss << RES_PATH << tempStr;
		}
		temp->displacementID = loadTexture(ss.str().c_str());

		material->Get(AI_MATKEY_TEXTURE_SPECULAR(0), string);
		tempStr = std::string(string.C_Str());
		ss.str(std::string());
		if (tempStr != "") {
			tempStr = tempStr.substr(tempStr.find_last_of("/")+1);
			ss << RES_PATH << tempStr;
		}
		temp->specularID = loadTexture(ss.str().c_str());
		
		material->Get(AI_MATKEY_SHININESS, temp->shineDamper);
		
		material->Get(AI_MATKEY_REFLECTIVITY, temp->reflectivity);
		textures[i] = temp;
	}
	for (size_t i = 0; i < scene->mNumMeshes; i++)
	{
		RawModel* temp;
		aiMesh* mesh = scene->mMeshes[i];
		GLfloat* vertices = (GLfloat*)malloc(sizeof(GLfloat)*mesh->mNumVertices * 3);
		GLfloat* texCoords = (GLfloat*)malloc(sizeof(GLfloat)*mesh->mNumVertices * 2);
		GLfloat* normals = (GLfloat*)malloc(sizeof(GLfloat)*mesh->mNumVertices * 3);
		GLint* tops = (GLint*)malloc(sizeof(GLint)*mesh->mNumVertices);
		GLuint* boneIDs = (GLuint*)malloc(sizeof(GLuint)*mesh->mNumVertices * 4);
		GLfloat* boneWeights = (GLfloat*)malloc(sizeof(GLfloat)*mesh->mNumVertices * 4);
		GLuint* indices = (GLuint*)malloc(sizeof(GLuint)*mesh->mNumFaces * 3);
		for (size_t j = 0; j < mesh->mNumVertices; j++)
		{
			vertices[j * 3 + 0] = mesh->mVertices[j].x;
			vertices[j * 3 + 1] = mesh->mVertices[j].y;
			vertices[j * 3 + 2] = mesh->mVertices[j].z;
			texCoords[j * 2 + 0] = mesh->mTextureCoords[0][j].x;
			texCoords[j * 2 + 1] = mesh->mTextureCoords[0][j].y;
			normals[j * 3 + 0] = mesh->mNormals[j].x;
			normals[j * 3 + 1] = mesh->mNormals[j].y;
			normals[j * 3 + 2] = mesh->mNormals[j].z;
			tops[j] = 0;

		}
		for (size_t j = 0; j < mesh->mNumBones; j++)
		{
			aiBone* bone = mesh->mBones[j];
			for (size_t k = 0; k < bone->mNumWeights; k++)
			{
				aiVertexWeight weight = bone->mWeights[k];
				if (tops[weight.mVertexId] == 4)
					continue;
				boneIDs[weight.mVertexId * 4 + tops[weight.mVertexId]] = necessityMap[bone->mName.C_Str()];//NodeID mit dem Namen des bone finden
				boneWeights[weight.mVertexId * 4 + tops[weight.mVertexId]] = weight.mWeight;
				tops[weight.mVertexId]++;
			}
		}
		for (size_t j = 0; j < mesh->mNumFaces; j++)
		{
			aiFace face = mesh->mFaces[j];
			indices[j * 3 + 0] = face.mIndices[0];
			indices[j * 3 + 1] = face.mIndices[1];
			indices[j * 3 + 2] = face.mIndices[2];
		}
		
		temp = loadToVAO(vertices, sizeof(GLfloat)*mesh->mNumVertices * 3, texCoords, sizeof(GLfloat)*mesh->mNumVertices * 2,
			normals, sizeof(GLfloat)*mesh->mNumVertices * 3, boneIDs, sizeof(GLuint)*mesh->mNumVertices * 4, 
			boneWeights, sizeof(GLfloat)*mesh->mNumVertices * 4,
			indices, sizeof(GLuint)*mesh->mNumFaces * 3);
		temp->setMaterialIndex(mesh->mMaterialIndex);
		models[i] = temp;
	}
	std::map<std::string, GLuint> animations;
	for (size_t i = 0; i < scene->mNumAnimations; i++)
	{
		aiAnimation* animation = scene->mAnimations[i];
		std::string name = std::string(animation->mName.C_Str());
		animations[name] = i;
	}
	TexturedModel* texModel = new TexturedModel(models, scene->mNumMeshes, textures, scene->mNumMaterials);
	//texModel->setSkeleton(skeleton);
	texModel->setScene(scene);
	texModel->animations = animations;
	return texModel;
}
/*void Loader::loadBones(std::vector<Bone>& skeleton, aiNode * node, int parent, std::map<const char*, int> necessityMap)
{
	for (GLuint i = 0; i < node->mNumChildren; i++)
	{
		Bone b = Bone();
		aiMatrix4x4 temp = node->mChildren[i]->mTransformation;
		b.nodeTransform = glm::mat4(temp.a1, temp.a2, temp.a3, temp.a4, 
								    temp.b1, temp.b2, temp.b3, temp.b4, 
			                        temp.c1, temp.c2, temp.c3, temp.c4, 
									temp.d1, temp.d2, temp.d3, temp.d4);
		b.parent = parent;
		skeleton.push_back(b);
		necessityMap[node->mName.C_Str()] = skeleton.size()-1;
		loadBones(skeleton, node->mChildren[i], skeleton.size() - 1, necessityMap);
	}
}*/

GLuint Loader::createVAO()
{
	GLuint vaoID;
	glGenVertexArrays(1, &vaoID);
	vaos.push_back(vaoID);
	glBindVertexArray(vaoID);
	return vaoID;
}
void Loader::storeDataInAttributeList(GLuint attributeNumber, GLfloat data[], GLuint length, GLint dimensions)
{
	GLuint vboID;
	glGenBuffers(1, &vboID);
	vbos.push_back(vboID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	glBufferData(GL_ARRAY_BUFFER, length, data, GL_STATIC_DRAW);
	glVertexAttribPointer(attributeNumber, dimensions, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
void Loader::storeDataInAttributeList(GLuint attributeNumber, GLuint data[], GLuint length, GLint dimensions)
{
	GLuint vboID;
	glGenBuffers(1, &vboID);
	vbos.push_back(vboID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	glBufferData(GL_ARRAY_BUFFER, length, data, GL_STATIC_DRAW);
	glVertexAttribIPointer(attributeNumber, dimensions, GL_UNSIGNED_INT, 0, (void*)0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
void Loader::cleanUp()
{
	std::list<GLuint>::iterator vaoB = vaos.begin();
	for (vaoB; vaoB != vaos.end();vaoB++) {
		glDeleteVertexArrays(1, &*vaoB);
	}
	std::list<GLuint>::iterator vboB = vbos.begin();
	for (vboB; vboB != vbos.end();vboB++) {
		glDeleteBuffers(1, &*vboB);
	}
	std::list<GLuint>::iterator texB = textures.begin();
	for (texB; texB != textures.end();texB++) {
		glDeleteTextures(1, &*texB);
	}

}
void Loader::unbindVAO()
{
	glBindVertexArray(0);
}

void Loader::bindIndicesBuffer(GLuint indices[], GLuint indicesLength)
{
	GLuint vboID;
	glGenBuffers(1, &vboID);
	vbos.push_back(vboID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesLength, indices, GL_STATIC_DRAW);
}
