#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <glm\glm.hpp>
#include "rawmodel.h"
#include "display.h"
#include "cam.h"
#include "loader.h"
#include "shader.h"

const float vertices[]{-1, -1, 0, 1, -1, 0, -1, 1, 0, 1, 1, 0};
const float texCoords[]{ 0, 1, 1, 1, 0, 0, 1, 0 };
int main()
{
	createDisplay();
	Camera* cam = new Camera(new glm::vec3(0, 0, 10));
	Loader* loader = new Loader();
	MasterSound* sound = new MasterSound();
}