#ifndef RAWMODEL_H
#define RAWMODEL_H
class RawModel
{
private:
	GLuint vaoID;
	GLsizei vertexCount;
	GLuint materialID;
	const char* name;
public:
	RawModel(GLuint vaoID, GLsizei vertexCount);
	RawModel(GLuint vaoID, GLsizei vertexCount, const char* name);
	GLuint getVaoID() { return vaoID; }
	GLuint getMaterialIndex() { return materialID; };
	void setMaterialIndex(GLuint materialID) { this->materialID = materialID; };
	GLsizei getVertexCount() { return vertexCount; }
};

#endif /* !RAWMODEL_H */