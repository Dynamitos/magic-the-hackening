#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H
#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <glm\glm.hpp>

class ShaderProgram
{
public:
	ShaderProgram(char* vertexShader, char* fragmentShader, char* tesselationControlShader, char* tesselationEvaluationShader);
	ShaderProgram(char* vertexShader, char* fragmentShader);
	~ShaderProgram();
	void start();
	void stop();
	void initShader();
protected:
	virtual void bindAttributes() = 0;
	virtual void getAllUniformLocations() = 0;
	void bindAttribute(GLuint attributeNumber, char* attributeName);
	GLint getUniformLocation(const char* uniformName);
	void loadMatrix(GLuint location, glm::mat4 matrix);
	void loadInt(GLuint location, GLint value);
	void loadFloat(GLuint location, GLfloat value);
	void loadBoolean(GLuint location, GLboolean value);
	void loadVector(GLuint location, glm::vec3 vector);
	void loadVector(GLuint location, GLfloat x, GLfloat y);
	void loadVector(GLuint location, glm::vec2 value);
	void loadVector(GLuint location, glm::vec4 value);
private:
	GLuint programID;
	GLuint vertexShaderID;
	GLuint fragmentShaderID;
	GLuint tesselationControlShaderID;
	GLuint tesselationEvaluationShaderID;
	static GLuint loadShader(char* file, GLuint type);
};
class EntityShader : public ShaderProgram
{
public:
	EntityShader();
	~EntityShader();

	void loadDisplacement(GLfloat displacementFactor);
	void loadOffset(GLfloat x, GLfloat y);
	void loadFog(GLfloat gradient, GLfloat density);
	void loadSkyColor(glm::vec3 skyColor);
	void loadFakeLighting(GLboolean useFake);
	void loadShineVariables(GLfloat damper, GLfloat reflectivity);
	void loadViewMatrix(Camera* camera);
	void loadTransformationMatrix(glm::mat4 matrix);
	void loadProjectionMatrix(glm::mat4 projectionMatrix);
	void loadPlane(glm::vec4 plane);
	void loadLights();
	void loadTextures();
	void loadBones(GLfloat* matrices, GLuint bones);
protected:
	void getAllUniformLocations();
	void bindAttributes();
private:
	GLuint location_transformationMatrix;
	GLuint location_projectionMatrix;
	GLuint location_viewMatrix;
	GLuint location_lightPosition[MAX_LIGHTS];
	GLuint location_lightColor[MAX_LIGHTS];
	GLuint location_reflectivity;
	GLuint location_shineDamper;
	GLuint location_useFakeLighting;
	GLuint location_skyColor;
	GLuint location_density;
	GLuint location_gradient;
	GLuint location_numberOfRows;
	GLuint location_offset;
	GLuint location_attenuation[MAX_LIGHTS];
	GLuint location_cameraPos;
	GLuint location_displacementFactor;
	GLuint location_plane;
	GLuint location_textureSampler;
	GLuint location_displacementSampler;
	GLuint location_bones;
};
class ParticleShader : public ShaderProgram
{
public:
	ParticleShader();
	void loadProjectionMatrix(glm::mat4& projectionMatrix);
	void loadTexture();
	void loadCamera(Camera* camera);
protected:
	void bindAttributes();
	void getAllUniformLocations();
private:
	GLuint location_cameraRight;
	GLuint location_cameraUp;
	GLuint location_projection;
	GLuint location_view;
	GLuint location_texture;
};

#endif /* !SHADERPROGRAM_H */